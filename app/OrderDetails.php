<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class OrderDetails extends Model
{
    //
    protected $table = 'order_details';

    protected $fillable = [
        'product_id','price','quantity','discount','total','product_sku','size',
        'color','shipdate','billdate','order_number','orders_id','pay_by','product_variant_id'];

    public function product()
	{
		return $this->hasMany('App\Product','id','product_id');
	}

	public function order()
	{
		return $this->belongsTo('App\Orders', 'id', 'orders_id');
	}
}
