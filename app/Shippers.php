<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shippers extends Model
{
    //
    protected $table = 'shippers';
    protected $fillable = [
        'name','contact_number'];

     public function order()
	{
		return $this->belongsTo('App\Orders', 'shipper_id', 'id');
	}
}
