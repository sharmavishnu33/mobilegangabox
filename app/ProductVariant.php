<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model
{
    //
     protected $table = "products_variants";

    public function productImage()
    {
    	return $this->hasOne('App\ProductImage', 'product_variant_id', 'id');
    }

    public function color()
    {
    	return $this->hasMany('App\Color', 'id', 'color_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product', 'id', 'product_id');
    }
    public function colorData()
    {
        return $this->hasOne('App\Color', 'id', 'color_id');
    }
    public function productData()
    {
        return $this->hasOne('App\Product', 'id', 'product_id');
    }
    public function branch()
    {
        return $this->hasOne('App\Branch', 'id', 'branch_id');
    }
    
}
