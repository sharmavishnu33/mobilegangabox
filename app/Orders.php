<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    //
    protected $table = 'orders';
    
    public function guestUser()
	{
		return $this->hasOne('App\GuestUserInfo','id','guest_user_id');
	}

	public function payment()
	{
		return $this->hasOne('App\Payment','id','payment_id');
	}

	public function shipping()
	{
		return $this->hasOne('App\Shippers','id','shipper_id');
	}

	public function orderDetails()
	{
		return $this->hasOne('App\OrderDetails', 'orders_id', 'id');
	}

	// public function productImage()
	// {
	// 	return $this->hasOne('App\ProductImage', 'orders_id', 'id');
	// }


}