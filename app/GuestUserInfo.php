<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuestUserInfo extends Model
{
    //
    protected $table = "guest_user_info";
    protected $fillable = [
        'name','email','state','mobile','postal_code','street','street_number','reference',
        'device_token'];


    
}
