<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    //
    protected $table = 'branch';

    public function country()
	{
		return $this->hasOne('App\Country', 'id', 'country_id');
	}
}
