<?php

namespace App\Http\Controllers\cart;

use App\Branch;
use App\Cart;
use App\GuestUserInfo;
use App\Http\Controllers\Controller;
use App\Http\Controllers\OrderController;
use App\Inventory;
use App\OrderDetails;
use App\Orders;
use App\PaymentDetail;
use App\Product;
use App\ProductVariant;
use App\Shippers;
use App\ShippingDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    //
    public function index(Request $request)
    {
        $value = $request->session()->getId();

        $carts = Cart::join('products', 'cart.product_id', '=', 'products.id')
            ->join('products_variants', 'cart.variant_id', '=', 'products_variants.id')
            ->select('cart.product_name','cart.id', 'cart.product_color', 'cart.description', 'cart.product_image', 'cart.product_price', 'cart.quantity','cart.product_size','products_variants.product_price as mainPrice')
            ->where('session_id', $value)->where('cart.status',0)
            ->orderBy('cart.product_id')
            ->get();
        $shoppingDetails = ShippingDetail::first();
//        dd($shoppingDetails);
        $products = Product::leftjoin('products_variants','products.id','=','products_variants.product_id')
            ->leftjoin('products_images','products_variants.id','=','products_images.product_variant_id')
            ->select('products.name as pname','products.id as pid','products_variants.id as vid','products_variants.product_price as product_price','products_images.images')
            ->groupBy('products.id')
            ->orderBy('products.id','asc')
            ->take(5)
            ->get();
        if(count($products) > 0) {
            $lastProductId = $products->last()->pid;
        }else{
            $lastProductId = null;
        }

        if (count($carts) > 0){

            $subtotal = Cart::where('session_id', $value)->select(DB::raw('sum(product_price) as price'))->first();

            return view('cart.mobile_cart',compact('carts','subtotal','products','shoppingDetails','lastProductId'));
    } else{
            $sessionId = $request->session()->getId();
            $carttotal = Cart::where('session_id', $sessionId)->select(DB::raw('count(id) as cartcount'))->where('status',0)->first();

            return view('cart.mobile_empty_cart',compact('products','carttotal','lastProductId'));

        }
//        dd($subtotal);

    }

    public function updateCart(Request $request)
    {
        $cart = Cart::find($request->cartid);
        $variant = Inventory::where('product_variant_id',$cart->variant_id)->first();
        $quantity=  $variant->in_stock > 0 ? $variant->in_stock : $variant->initial_stock;
//        dd($quantity);
        if($quantity >= $request->qunty) {
//            dd('dd');
            $cart->quantity = $request->qunty;
            $cart->product_price = $request->subtotal;
            $cart->save();
            return response()->json([
                'success' => true
            ]);
        }
        else
        {
            return response()->json([
                'success' => false,'quantity' => $quantity
            ]);
        }
    }

    public function removeCart(Request $request)
    {
        $cart = Cart::where('id',$request->cartid)->delete();
        return response()->json([
            'success' => true
        ]);
    }

    public function guestConfirm(Request $request)
    {
        $total = $request->total;
//        dd($total);
        $sessionId = $request->session()->getId();
        $carts = Cart::join('products', 'cart.product_id', '=', 'products.id')
            ->join('products_variants', 'cart.variant_id', '=', 'products_variants.id')
            ->select('cart.product_name','cart.id', 'cart.product_color', 'cart.description', 'cart.product_image', 'cart.product_price', 'cart.quantity','cart.product_size','products_variants.product_price as mainPrice')
            ->where('session_id', $sessionId)->where('cart.status',0)->get();
//        dd($carts);

        $subtotal = Cart::where('session_id', $sessionId)->select(DB::raw('sum(product_price) as price'))->first();
        $state = Branch::all();
        return view('cart.guestinfo',compact('total','subtotal','carts','state'));
    }

    public function orderConfirm(Request $request)
    {
        $value = $request->session()->getId();
//        dd($request);

        $guestInfo = new GuestUserInfo;
        $guestInfo->name= $request->fullname;
        $guestInfo->email= $request->email;
        $guestInfo->state= $request->state;
        $guestInfo->mobile_code= $request->mobile_code;
        $guestInfo->mobile= $request->mobile_number;
        $guestInfo->postal_code= $request->postal_code;
        $guestInfo->street= $request->street;
        $guestInfo->street_number= $request->street_number;
        $guestInfo->reference= $request->reference;
        $guestInfo->session_id= $value;
        $guestInfo->save();
//
       $orderNumber = OrderDetails::where('order_number','200000')->first();

       if($orderNumber)
       {
          $orderData = OrderDetails::all()->last();
          $orderNumberData = $orderData->order_number;
           $orderNumberData = $orderNumberData + 1;
           $random = $orderNumberData;
       }else{
           $random= 200000;
       }


//       if($orderNumber->order_number >)
//       dd($orderNumber->order_number);
//       dd($orderNumber);
//        $order_number = rand(10000, 99999);
//        $t=time();
//        $random = 200000;

        $order_number = $random;
        $shippersData = array('name'=>'test name' , 'contact_number' =>'1234567890');
        $todayDate = date("Y-m-d");
        $shippers = Shippers::create($shippersData);
        $cartIds = $request->cartId;
        $orderDetails = '';
//        dd(count($cartIds));
        foreach ($cartIds as $cartId)
        {
            $cart = Cart::find($cartId);
            $product = Product::where('id', $cart->product_id)->with('productVariant')->first();
            $variant = ProductVariant::where('id', $cart->variant_id)->first();
            $cart->status = 1;
            $cart->save();
//            dd($product);
            $orderData = new Orders;
            $orderData->guest_user_id = $guestInfo->id;
            $orderData->shipper_id = $shippers->id;
            $orderData->order_number = $order_number;
            $orderData->order_date = Carbon::now();
            $orderData->ship_date = date("Y-m-d", strtotime($todayDate . '+ 10 days'));
            $orderData->delivery_date = date("Y-m-d", strtotime($todayDate . '+ 11 days'));
            $orderData->sales_tax = $request->tax;
            $orderData->status = 0;
            $orderData->save();

            $data = array('product_id' => $product->id, 'price' => $cart->product_price, 'quantity' => $cart->quantity,
                'discount' => 0, 'total' => $request->total, 'product_sku' => $product->productVariant->product_sku, 'size' => $cart->product_size
            , 'color' => $cart->product_color,'product_variant_id' =>$variant->id, 'shipdate' => $orderData->ship_date, 'billdate' => $orderData->delivery_date, 'order_number' => $order_number, 'orders_id' => $orderData->id);
//                    dd($data);
          $orderDetails =  OrderDetails::create($data);
//            dd($orderDetails);

        }
//        dd($orderDetails->order_number);
        $request->session()->put('orderNumber', $orderDetails->order_number);
        OrderController::checkout_process($orderDetails->order_number);
        $request->session()->put('total', $request->total);
//        Session::set('orderNumber', $orderDetails->order_number);
        return Redirect::route('agree.confirm');
//        return view('cart.thanku_page');
    }

    public function agreeConfirm(Request $request)
    {
        $orderNumber = Session::get('orderNumber');
        $total = Session::get('total');
//        dd($total);
        $orderdata =  Orders::where('order_number',$orderNumber)->first();
        $guestId =  $orderdata->guest_user_id;
        $shipping = ShippingDetail::first();
//        dd($shipping);
        $guestInfo = GuestUserInfo::find($guestId);
        $paymentDetails = PaymentDetail::first();


        return view('cart.mobile_checkout',compact('guestInfo','orderNumber','total','shipping','paymentDetails'));
    }
    public function updateConfirm(Request $request)
    {
        $orderNumber = Session::get('orderNumber');
        $payBy = $request->pay_by;
        $data = array('pay_by' =>$payBy);
        $orderDetails = OrderDetails::where('order_number',$orderNumber)->update($data);
        return Redirect::route('thanks.page');

    }

    public function thanksPage(Request $request)
    {
        $sessionId = $request->session()->getId();
        $orderNumber = Session::get('orderNumber');
        $carts = Cart::join('products', 'cart.product_id', '=', 'products.id')
            ->join('products_variants', 'cart.variant_id', '=', 'products_variants.id')
            ->select('cart.product_name','cart.id', 'cart.product_color', 'cart.description', 'cart.product_image', 'cart.product_price', 'cart.quantity','cart.product_size','products_variants.product_price as mainPrice')
            ->where('session_id', $sessionId)->get();
//        dd($carts);
        $subtotal = Cart::where('session_id', $sessionId)->select(DB::raw('sum(product_price) as price'))->first();
        $orderdata =  Orders::where('order_number',$orderNumber)->first();
        $guestId =  $orderdata->guest_user_id;
        $user = GuestUserInfo::find($guestId);
//        dd($user);
        $orderDetails = OrderDetails::where('order_number',$orderNumber)->first();
//        dd($orderDetails);
        return view('cart.mobile_thanku_page',compact('carts','subtotal','orderNumber','user','orderDetails'));
    }

    public static function cartTotal(Request $request)
    {
        $sessionId = $request->session()->getId();
        $cart = Cart::where('session_id', $sessionId)->select(DB::raw('count(id) as cartcount'))->first();
        return $cart;
    }

    public function colorSize(Request $request)
    {
        $colorSizes = ProductVariant::where('color_id',$request->color_id)->where('product_id',$request->product_id)->select('size','quantity')->groupBy('size')->get();
//        dd($colorSizes);
        $sizeData = ProductVariant::where('color_id',$request->color_id)->where('product_id',$request->product_id)->first();

        $data = view('cart.test',compact('colorSizes'))->render();
        if($sizeData)
        {
           $price_data =  $sizeData->product_price;
        }else{
            $price_data =  0;
        }
//        dd($data);
        return response()->json([
            'success' => 'success','data'=>$data,'price_data' =>$price_data
        ]);
    }

    public function getDetails(Request $request)
    {
        $sessionId = $request->session()->getId();
        $carttotal = Cart::where('session_id', $sessionId)->select(DB::raw('count(id) as cartcount'))->where('status',0)->first();
        $branches = Branch::all();
        $data = view('product.branch',compact('branches'))->render();
        return response()->json([
            'success' => 'success','data'=>$data,'cartTotal' => $carttotal
        ]);

    }

    public function productPrice(Request $request)
    {
        $productSize = ProductVariant::where('color_id',$request->colorid)
            ->where('product_id',$request->productId)
            ->where('size',$request->size)
            ->first();
        return response()->json([
            'success' => 'success','data'=>$productSize->product_price
        ]);
    }

}
