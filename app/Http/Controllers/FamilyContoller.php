<?php


namespace App\Http\Controllers;


use App\Family;
use Illuminate\Http\Request;
use DB;

class FamilyContoller
{

    public static function familyData()
    {
        $families = Family::select('id','name')->get();
        return $families;
    }
    public function mobileProduct(Request $request)
    {
        $request->session()->put('family_id', $request->id);
        $families_subcategory = DB::table('family')->where('family.id', $request->id)
            ->leftjoin('category_subcategory', 'family.id', '=', 'category_subcategory.family_id')
            ->leftJoin('categories', 'category_subcategory.categories_id', '=', 'categories.id')
            ->leftJoin('subcategory_products', 'categories.id', '=', 'subcategory_products.category_id')
            ->leftjoin('products', 'subcategory_products.product_id', '=', 'products.id')
            ->join('products_variants', 'products.id', '=', 'products_variants.product_id')
            ->join('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
            ->select('family.id as category_id', 'family.name as category_name', 'category_subcategory.family_id as cs_category_id',
                'category_subcategory.categories_id as cs_subcategory_id', 'categories.id as subcat_id',
                'categories.cat_name as subcat_name', 'subcategory_products.category_id as sp_subcat_id',
                'subcategory_products.product_id as sp_product_id', DB::raw('GROUP_CONCAT(products.name) as productname'),
                DB::raw('GROUP_CONCAT(products.id) as productid'))
            ->groupBy('subcategory_products.product_id')
            // ->get();
            ->select('family.id as family_id', 'family.name as family_name', 'categories.id as cat_id','products.id as pid', 'products.name as product_name', 'products.description', 'products_variants.product_price', 'products_images.images')->get();
        $data = view('product.productlist',compact('families_subcategory'))->render();
        return response()->json([
            'success' => 'success','data'=>$data
        ]);
    }
}
