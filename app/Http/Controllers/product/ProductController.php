<?php

namespace App\Http\Controllers\product;

use App\Branch;
use App\Cart;
use App\CategorySubcategory;
use App\Color;
use App\Family;
use App\Http\Controllers\Controller;
use App\Images;
use App\Inventory;
use App\Product;
use App\ProductImage;
use App\ProductVariant;
use App\SubcategoryProducts;
use App\Traits\CommonTrait;
use Illuminate\Http\Request;
use DB;
use Response;


class ProductController extends Controller
{
    //
    public function index(Request $request){
        $value = $request->session()->getId();
        $families = Family::all();
        $familySubcats= array();
        foreach ($families as $family)
        {
            $subfam = CategorySubcategory::where('family_id',$family->id)->with('category')->get();
            $familySubcats[] =['fname'=>$family->name,'fid'=>$family->id,'subcat' =>$subfam];
        }
        $products = Product::leftjoin('products_variants','products.id','=','products_variants.product_id')
            ->leftjoin('products_images','products_variants.id','=','products_images.product_variant_id')
            ->select('products.name as pname','products.id as pid','products_variants.id as vid',
                'products_variants.product_price as product_price','products_images.images')
            ->groupBy('products.id')
            ->orderBy('products.id','asc')
            ->take(20)
            ->get();
        if(count($products) > 0) {
            $lastProductId = $products->last()->pid;
        }else{
            $lastProductId = null;
        }
        $sessionId = $request->session()->getId();
        $carttotal = Cart::where('session_id', $sessionId)->select(DB::raw('count(id) as cartcount'))->where('status',0)->first();

//        dd($request->route()->getName());
    	return view('product.mobile_product_list',compact('familySubcats','products','carttotal','lastProductId'));
    }

    public function varianSize($id,$product_id)
    {
        $sizes = ProductVariant::where('color_id',$id)->where('product_id',$product_id)->select('size')->groupBy('size')->get()->toArray();
        return $sizes;
    }

    public function productDescription(Request $request,$id)
    {
        $productId = CommonTrait::decodeId($id);
        $pvariant = Product::where('id',$productId)->with('productVariant')->first();
        $varients = ProductVariant::where('product_id',$productId)->with('colorData')->with('productImage','branch')->get();
        $colors = ProductVariant::where('product_id',$productId)->with('colorData')->with('productImage','branch')->groupBy('color_id')->get();
//        dd($varients);
        $productImage = Images::where('product_id',$productId)->get();
        $products = Product::leftjoin('products_variants','products.id','=','products_variants.product_id')
            ->leftjoin('products_images','products_variants.id','=','products_images.product_variant_id')
            ->select('products.name as pname','products.id as pid','products_variants.id as vid','products_variants.product_price as product_price','products_images.images')
//            ->with('products_variants.branch')
            ->groupBy('products.id')
            ->get();
        $sessionId = $request->session()->getId();
        $carttotal = Cart::where('session_id', $sessionId)->select(DB::raw('count(id) as cartcount'))->where('status',0)->first();

//        dd($varients);
        return view('product.mobile_product',compact('pvariant','varients','productImage','products','carttotal','colors'));

    }

    public function subCatProduct(Request $request)
    {
        $subId = $request->subcatid;
//        dd($subId);
//        $subProduct = SubcategoryProducts::where('')->get();
        $products = Product::leftjoin('products_variants','products.id','=','products_variants.product_id')
            ->leftjoin('products_images','products_variants.id','=','products_images.product_variant_id')
            ->leftjoin('subcategory_products','products.id','=','subcategory_products.product_id')
            ->select('products.name as pname','products.id as pid','products_variants.id as vid','products_variants.product_price as product_price','products_images.images')
            ->where('subcategory_products.category_id',$subId)
            ->orderBy('products.id','desc')
            ->groupBy('products.id')
            ->get();
//        $products = SubcategoryProducts::leftjoin('products','subcategory_products.product_id','=','products.id')
//            ->join('products_variants','products.id','=','products_variants.product_id')
//            ->join('products_images','products_variants.id','=','products_images.product_variant_id')
//            ->select('products.name as pname','products.id as pid','products_variants.id as vid','products_variants.product_price as product_price','products_images.images')
//            ->where('subcategory_products.category_id',$subId)
//            ->groupBy('subcategory_products.product_id')
//            ->get();
//        dd($products);
//        $products_item    s = json_encode($products);
//        dd($products_items);

        $data = view('product.sub_product_list',compact('products'))->render();
        return response()->json([
            'success' => 'success','data'=>$data
        ]);
    }

    public function imageView(Request $request)
    {
        $imageid = $request->imageid;
        $productImage = ProductImage::find($imageid);
        $data = view('product.image_view',compact('productImage'))->render();
        return response()->json([
            'success' => 'success','data'=>$data
        ]);
    }

    public function productSize(Request $request)
    {
        $colorId = $request->color;
        $productId = $request->productId;
        $productSizes = ProductVariant::where('product_id',$productId)->where('color_id',$colorId)->select('size')->groupBy('color_id')->get();
        $data = view('product.image_sizes',compact('productSizes'))->render();
        return response()->json([
            'success' => 'success','data'=>$data
        ]);
    }

    public function addCart(Request $request)
    {
//        dd($request);
        $value = $request->session()->getId();
        $ip = $request->ip();
        $productId = $request->productId;
        $colorId = $request->colorId;
        $colorSize = $request->colorSize;
        $products = Product::find($productId);
        $color = Color::find($colorId);
        $variants = ProductVariant::where('color_id',$colorId)->where('product_id',$productId)->where('size',$colorSize)->first();
//        dd($variants);
        $InstockData = Inventory::where('product_variant_id',$variants->id)->first();
        if ($InstockData) {
            $quantity = $InstockData->in_stock > 0 ? $InstockData->in_stock : $InstockData->initial_stock;

            if ($request->quanti <= $quantity) {
//                dd($ip);
                $ipAddress = $request->ip();
                $cartExist = Cart::where('variant_id', $variants->id)->where('product_id', $productId)->where('status', 0)->where('ip_address', $ipAddress)->first();
                if ($cartExist) {
                    $cartExist->quantity = $cartExist->quantity + $request->quanti;
                    $cartExist->product_price = ($variants->product_price * $cartExist->quantity);

                    $cartExist->save();
                } else {
                    $productImage = ProductImage::where('product_variant_id', $variants->id)->first();

                    $carts = new cart;
                    $carts->product_name = $products->name;
                    $carts->product_price = ($variants->product_price * $request->quanti);
                    $carts->description = $products->description;
                    $carts->quantity = $request->quanti;
                    $carts->product_color = $color->color_name;
                    $carts->product_id = $productId;
                    $carts->product_size = $colorSize;
                    $carts->session_id = $value;
                    $carts->ip_address = $ip;
                    $carts->variant_id = $variants->id;
//        dd('http://gangabox.test/storage/images/' . $productImage->images);
                    if (env('APP_ENV') == 'local') {
                        $carts->product_image = 'http://gangabox.test/storage/images/' . $productImage->images;
                    } else {
                        $carts->product_image = 'https://arrivederci.mx/storage/images/' . $productImage->images;

                    }

                    $carts->save();
//            dd($carttotal);

                }
                $carttotal = Cart::where('ip_address', $ip)->select(DB::raw('count(id) as cartcount'))->where('status', 0)->first();

                return response()->json([
                    'success' => true, 'cartNumber' => $carttotal->cartcount
                ]);
            }else {
                return response()->json([
                    'success' => false, 'quantity' => $quantity
                ]);
            }
        }
        else {
            return response()->json([
                'success' => false, 'quantity' => 0
            ]);
        }
    }

}
