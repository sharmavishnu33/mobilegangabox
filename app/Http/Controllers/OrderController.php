<?php

namespace App\Http\Controllers;

use App\ProductVariant;
use Illuminate\Http\Request;
use App\Orders;
use App\OrderDetails;
use App\Shippers;
use App\Payment;
use DB;
use App\Exports\OrdersExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use App\Inventory;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $orders = Orders::with('guestUser', 'payment', 'shipping', 'orderDetails', 'orderDetails.product','orderDetails.product')->get();
         //dd($orders);
        return view('admin.order_list', compact('orders'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $orders = Orders::with('guestUser', 'shipping', 'orderDetails', 
        'orderDetails.product', 'orderDetails.product.productVariant', 
        'orderDetails.product.productVariant.productImage')->where('id',$id)->get();
       // dd($orders);
        return view('admin.order_view', compact('orders'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $orderDetails = OrderDetails::where('order_number',$orderNumber)->get();
        $orders = Orders::findOrFail($id);
        $orders->status = $request->status;
        $orders->update();
        foreach ($orderDetails as $orderDetail)
        {
            $variantId = $orderDetail->product_variant_id;
            dd($variantId);
            $invtoryData = Inventory::where('product_variant_id',$variantId)->first();
//            dd($invtoryData);
            if ($orders->status == 5) {
            $sales_qty = (int)$invtoryData->quantity - (int)$orderDetail->sale_qty;
            $actual_stock = (int)$invtoryData->initial_stock + (int)$sales_qty;
            $original_price = $invtoryData->sale_cost;
            $order_price = $orderDetail->price;
            $sales_price = $original_price - $order_price;
            $sale_date = $orderDetail->created_at;
            $invtoryData->in_stock = $actual_stock;
            $invtoryData->sale_qty = $sales_qty;
            $invtoryData->sale_cost = $sales_price;
            $invtoryData->sale_date = $sale_date;
            $invtoryData->save();
           }
          

        }
        return back()->with('status', 'Order Status Changed Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function export(Request $request) 
    {
        // dd($request->ids);
        $ids = $request->ids;
        $orderId = explode(',',$ids);
        $orders = DB::table('order_details')
                     ->whereIn('orders.id', $orderId)  
                     ->leftJoin('orders', 'order_details.orders_id', '=', 'orders.id')
                     ->join('guest_user_info', 'orders.guest_user_id', '=', 'guest_user_info.id')
                     ->join('shippers', 'orders.shipper_id', '=', 'shippers.id')
                     ->leftJoin('payment', 'orders.payment_id', '=', 'payment.id')
                    ->select('orders.order_number','orders.order_date','guest_user_info.name','guest_user_info.email',
                    'guest_user_info.street','guest_user_info.exterior_number','guest_user_info.reference',
                    'guest_user_info.state','guest_user_info.postal_code','guest_user_info.street_number',
                    'guest_user_info.mobile','orders.total','orders.status')
                      ->get();
            
        return Excel::download(new OrdersExport($orders), 'order_details.xlsx');
    }

    public function prvpriview()
      {
        $orders = Orders::with('guestUser', 'payment', 'shipping', 'orderDetails', 'orderDetails.product','orderDetails.product')->get();
         return view('admin.lists_order')->with('orders', $orders);
      
     }

     public static function checkout_process($orderNumber)
     {
        $orderDetails = OrderDetails::where('order_number',$orderNumber)->get();
        foreach ($orderDetails as $orderDetail)
        {
            $variantId = $orderDetail->product_variant_id;
            $invtoryData = Inventory::where('product_variant_id',$variantId)->first();
            
            $sales_qty = (int)$invtoryData->sale_qty + (int)$orderDetail->quantity;
            $actual_stock = (int)$invtoryData->initial_stock - (int)$sales_qty;
            $original_price = $invtoryData->sale_cost;
            $order_price = $orderDetail->price;
            $sales_price = $original_price + $order_price;
            $sale_date = $orderDetail->created_at;
            $invtoryData->in_stock = $actual_stock;
            $invtoryData->sale_qty = $sales_qty;
            $invtoryData->sale_cost = $sales_price;
            $invtoryData->sale_date = $sale_date;
            $invtoryData->save();

             $var_update = ProductVariant::where('id',$invtoryData->product_variant_id)
           ->update(['quantity' => $actual_stock]);
            
          
           

        }
    }
    
}
