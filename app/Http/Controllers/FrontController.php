<?php


namespace App\Http\Controllers;


use App\Banner;
use App\Branch;
use App\Cart;
use App\CategorySubcategory;
use App\Family;
use App\Product;
use App\Traits\CommonTrait;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FrontController extends Controller
{

    public function index(Request $request)
    {
        $sessionId = $request->session()->getId();
        $carttotal = Cart::where('session_id', $sessionId)->select(DB::raw('count(id) as cartcount'))->where('status',0)->first();
        $banners = Banner::all();
        // $families = Family::all();
        $families = Family::select('id','name')->get();
        $branch = Session::get('branch_id');


        $families_subcategory = DB::table('family')->leftjoin('category_subcategory', 'family.id', '=', 'category_subcategory.family_id')->leftJoin('categories', 'category_subcategory.categories_id', '=', 'categories.id')
            ->leftJoin('subcategory_products', 'categories.id', '=', 'subcategory_products.category_id')
            ->leftjoin('products', 'subcategory_products.product_id', '=', 'products.id')
            ->join('products_variants', 'products.id', '=', 'products_variants.product_id')
            ->join('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
            ->select('family.id as category_id', 'family.name as category_name',
                'category_subcategory.family_id as cs_category_id', 'category_subcategory.categories_id as cs_subcategory_id',
                'categories.id as subcat_id', 'categories.cat_name as subcat_name','products.name as product_name',
                'products_images.images','products_variants.product_price', 'subcategory_products.category_id as sp_subcat_id',
                'subcategory_products.product_id as sp_product_id', DB::raw('GROUP_CONCAT(products.name) as productname'),
                DB::raw('GROUP_CONCAT(products.id) as productid'))
            ->groupBy('subcategory_products.product_id')
            //->get();
            ->select('family.id as family_id', 'family.name as family_name', 'categories.id as cat_id','products.id as pid', 'products.name as product_name', 'products.description', 'products_variants.product_price', 'products_images.images')->get();
//dd($families_subcategory);
        $family = Family::first();
        $request->session()->put('family_id', $family->id);

        $products = DB::table('family')->where('family.id', $family->id)
            ->leftjoin('category_subcategory', 'family.id', '=', 'category_subcategory.family_id')
            ->leftJoin('categories', 'category_subcategory.categories_id', '=', 'categories.id')
            ->leftJoin('subcategory_products', 'categories.id', '=', 'subcategory_products.category_id')
            ->leftjoin('products', 'subcategory_products.product_id', '=', 'products.id')
            ->join('products_variants', 'products.id', '=', 'products_variants.product_id')
            ->join('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
            ->select('family.id as category_id', 'family.name as category_name', 'category_subcategory.family_id as cs_category_id', 'category_subcategory.categories_id as cs_subcategory_id', 'categories.id as subcat_id', 'categories.cat_name as subcat_name', 'subcategory_products.category_id as sp_subcat_id', 'subcategory_products.product_id as sp_product_id', DB::raw('GROUP_CONCAT(products.name) as productname'), DB::raw('GROUP_CONCAT(products.id) as productid'))->groupBy('subcategory_products.product_id')
            // ->get();
            ->select('family.id as family_id', 'family.name as family_name', 'categories.id as cat_id','products.id as pid', 'products.name as product_name', 'products.description', 'products_variants.product_price', 'products_images.images')
            ->when($branch ,function ($products) use($branch){
                $products->where('products_variants.branch_id',$branch);
            })
            ->orderBy('products.id','asc')
            ->take(20)
            ->get();
        if(count($products) > 0) {
            $lastProductId = $products->last()->pid;
        }else{
            $lastProductId = null;
        }
//        dd($products);
        return view('index',compact('banners','families','families_subcategory','carttotal','products','lastProductId'));
        //}

    }

    public function branchProductList(Request $request,$name)
    {

        $branch = Branch::where('name',$name)->first();
//        dd($branch);
        $familyId = Session::get('family_id');
//        dd($familyId);
        $request->session()->put('branch_id', $branch->id);
         $families = Family::all();
        $familySubcats= array();
        foreach ($families as $family)
        {
            $subfam = CategorySubcategory::where('family_id',$family->id)->with('category')->get();
            $familySubcats[] =['fname'=>$family->name,'fid'=>$family->id,'subcat' =>$subfam];
        }
        $products = Product::leftjoin('products_variants','products.id','=','products_variants.product_id')
            ->leftjoin('products_images','products_variants.id','=','products_images.product_variant_id')
            ->leftjoin('subcategory_products','products.id','=','subcategory_products.product_id')
            ->select('products.name as pname','products.id as pid','products_variants.id as vid','products_variants.product_price as product_price','products_images.images')
            ->where('products_variants.branch_id',$branch->id)
            ->groupBy('products.id')
            ->orderBy('products.id','asc')
            ->take(20)
            ->get();
        if(count($products) > 0) {
            $lastProductId = $products->last()->pid;
        }else{
            $lastProductId = null;
        }
        $sessionId = $request->session()->getId();
        $carttotal = Cart::where('session_id', $sessionId)->select(DB::raw('count(id) as cartcount'))->where('status',0)->first();
        return view('product.productlist',compact('familySubcats','products','carttotal','lastProductId'));

//        $data = view('product.productlist',compact('families_subcategory'))->render();
//        return response()->json([
//            'success' => 'success','data'=>$data
//        ]);
    }

    public function getFamilySub(Request $request)
    {
        $families = Family::all();
        $familySubcats= array();
        foreach ($families as $family)
        {
            $subfam = CategorySubcategory::where('family_id',$family->id)->with('category')->get();
            $familySubcats[] =['fname'=>$family->name,'fid'=>$family->id,'subcat' =>$subfam];
        }
        $data = view('product.family_sub',compact('familySubcats'))->render();
        return response()->json([
            'success' => 'success','data'=>$data
        ]);
    }

    public function familyProductList(Request $request,$subId,$fid)
    {
        $subcatId = CommonTrait::decodeId($subId);
        $familyId = CommonTrait::decodeId($fid);
        $branch = Session::get('branch_id');
        $families = Family::all();
        $familySubcats= array();
        foreach ($families as $family)
        {
            $subfam = CategorySubcategory::where('family_id',$family->id)->with('category')->get();
            $familySubcats[] =['fname'=>$family->name,'fid'=>$family->id,'subcat' =>$subfam];
        }
        $request->session()->put('category_id', $subcatId);

        $products = Product::leftjoin('products_variants','products.id','=','products_variants.product_id')
            ->leftjoin('products_images','products_variants.id','=','products_images.product_variant_id')
            ->leftjoin('subcategory_products','products.id','=','subcategory_products.product_id')
            ->select('products.name as pname','products.id as pid','products_variants.id as vid','products_variants.product_price as product_price','products_images.images')
            ->where('subcategory_products.category_id',$subcatId)
            ->groupBy('products.id')
            ->when($branch, function ($products) use($branch){
                $products->where('products_variants.branch_id',$branch);
            })
            ->orderBy('products.id')
            ->take(20)
            ->get();
        if(count($products) > 0) {
            $lastProductId = $products->last()->pid;
        }else{
            $lastProductId = null;
        }
        $sessionId = $request->session()->getId();
        $carttotal = Cart::where('session_id', $sessionId)->select(DB::raw('count(id) as cartcount'))->where('status',0)->first();

        return view('product.mobile_product_list',compact('familySubcats','products','carttotal','lastProductId'));

    }

    public function getMoreData(Request $request)
    {
        $pageNo =20;
        $branch = Session::get('branch_id');
//        dd($request->currentriute);

//        dd($request->currentriute);
        if($request->currentriute == 'home') {
            $familyId = Session::get('family_id');

            if(!empty($request->lastId)) {
                $products = DB::table('family')->where('family.id', $familyId)
                    ->leftjoin('category_subcategory', 'family.id', '=', 'category_subcategory.family_id')
                    ->leftJoin('categories', 'category_subcategory.categories_id', '=', 'categories.id')
                    ->leftJoin('subcategory_products', 'categories.id', '=', 'subcategory_products.category_id')
                    ->leftjoin('products', 'subcategory_products.product_id', '=', 'products.id')
                    ->join('products_variants', 'products.id', '=', 'products_variants.product_id')
                    ->join('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
                    ->select('family.id as category_id', 'family.name as category_name', 'category_subcategory.family_id as cs_category_id', 'category_subcategory.categories_id as cs_subcategory_id', 'categories.id as subcat_id', 'categories.cat_name as subcat_name', 'subcategory_products.category_id as sp_subcat_id', 'subcategory_products.product_id as sp_product_id', DB::raw('GROUP_CONCAT(products.name) as productname'), DB::raw('GROUP_CONCAT(products.id) as productid'))->groupBy('subcategory_products.product_id')
                    // ->get();
                    ->select('family.id as family_id', 'family.name as family_name', 'categories.id as cat_id','products.id as pid', 'products.name as product_name', 'products.description', 'products_variants.product_price', 'products_images.images')
                    ->when($branch ,function ($products) use($branch){
                        $products->where('products_variants.branch_id',$branch);
                    })
                    ->orderBy('products.id')
                    ->take($pageNo)
                    ->get();
                if(count($products) > 0) {
                    $lastProductId = $products->last()->pid;
                }else{
                    $lastProductId = null;
                }
            }else{
                $lastProductId = null;
                $products = [];
            }
        }
        elseif($request->currentriute == 'family.product.list')
        {
            $subcatId = Session::get('category_id');
            if(!empty($request->lastId)) {
                $products = Product::leftjoin('products_variants', 'products.id', '=', 'products_variants.product_id')
                    ->leftjoin('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
                    ->leftjoin('subcategory_products', 'products.id', '=', 'subcategory_products.product_id')
                    ->select('products.name as pname', 'products.id as pid', 'products_variants.id as vid', 'products_variants.product_price as product_price', 'products_images.images')
                    ->where('subcategory_products.category_id', $subcatId)
                    ->where('products.id', '>', $request->lastId)
                    ->take($pageNo)
                    ->groupBy('products.id')
                    ->orderBy('products.id', 'asc')
                    ->when($branch, function ($products) use ($branch) {
                        $products->where('products_variants.branch_id', $branch);
                    })
                    ->get();
                if (count($products) > 0) {
                    $lastProductId = $products->last()->pid;
                } else {
                    $lastProductId = null;
                }
            }else{
                $lastProductId = null;
                $products = [];
            }
        }
        elseif($request->currentriute == 'subcat.products')
        {
            $subcatId = Session::get('category_id');
            if(!empty($request->lastId)) {
                $products = Product::leftjoin('products_variants', 'products.id', '=', 'products_variants.product_id')
                    ->leftjoin('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
                    ->leftjoin('subcategory_products', 'products.id', '=', 'subcategory_products.product_id')
                    ->select('products.name as pname', 'products.id as pid', 'products_variants.id as vid', 'products_variants.product_price as product_price', 'products_images.images')
                    ->where('subcategory_products.category_id', $subcatId)
                    ->where('products.id', '>', $request->lastId)
                    ->take($pageNo)
                    ->groupBy('products.id')
                    ->orderBy('products.id', 'asc')
                    ->when($branch, function ($products) use ($branch) {
                        $products->where('products_variants.branch_id', $branch);
                    })
                    ->get();
                if (count($products) > 0) {
                    $lastProductId = $products->last()->pid;
                } else {
                    $lastProductId = null;
                }
            }else{
                $lastProductId = null;
                $products = [];
            }
        }
        elseif($request->currentriute == 'get.branch.product')
        {
            $branchId = Session::get('branch_id');
            if(!empty($request->lastId)) {
                $products = Product::leftjoin('products_variants', 'products.id', '=', 'products_variants.product_id')
                    ->leftjoin('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
                    ->select('products.name as pname', 'products.id as pid', 'products_variants.id as vid', 'products_variants.product_price as product_price', 'products_images.images')
                    ->where('products_variants.branch_id', $branchId)
                    ->where('products.id', '>', $request->lastId)
                    ->take($pageNo)
                    ->groupBy('products.id')
                    ->orderBy('products.id', 'asc')
                    ->when($branch, function ($products) use ($branch) {
                        $products->where('products_variants.branch_id', $branch);
                    })
                    ->get();
                if (count($products) > 0) {
                    $lastProductId = $products->last()->pid;
                } else {
                    $lastProductId = null;
                }
            }else{
                $lastProductId = null;
                $products = [];
            }

        }

        else{
            if(!empty($request->lastId)) {
                $products = Product::leftjoin('products_variants', 'products.id', '=', 'products_variants.product_id')
                    ->leftjoin('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
                    ->select('products.name as pname', 'products.id as pid', 'products_variants.id as vid', 'products_variants.product_price as product_price', 'products_images.images')
                    ->where('products.id', '>', $request->lastId)
                    ->groupBy('products.id')
                    ->take($pageNo)
                    ->orderBy('products.id', 'asc')
                    ->when($branch, function ($products) use ($branch) {
                        $products->where('products_variants.branch_id', $branch);
                    })
                    ->get();
                if (count($products) > 0) {
                    $lastProductId = $products->last()->pid;
                } else {
                    $lastProductId = null;
                }            }else{
                $lastProductId = null;
                $products = [];
            }
        }
//
            $data = view('product.productscroll', compact('products'))->render();
        return response()->json([
            'success' => 'success','data'=>$data,'lastProductId' =>$lastProductId
        ]);

    }
}
