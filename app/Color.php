<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    //
      protected $table = "colors";
      protected $fillable = ['color_name'];

    public function productVariant()
    {
    	return $this->belongsTo('App\productVariant', 'color_id', 'id');
    }
}
