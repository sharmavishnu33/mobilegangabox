<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    //
     protected $table = "products_images";

    public function productVariant()
    {
    	return $this->belongsTo('App\ProductVariant', 'id', 'product_variant_id');
    }

 //    public function order()
	// {
	// 	return $this->belongsTo('App\Orders', 'id', 'orders_id');
	// }

}
