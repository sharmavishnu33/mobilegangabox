@include('front_layouts.header')
<style>
   .num input {
      width: 30px;
      text-align: center;
      font-size: 18px;
      color: #a79f9f;
      border: 1px solid rgb(0, 0, 0);
      display: inline-block;
      vertical-align: baseline;
   }
   .num {
      margin-top: 5px;
   }
   span.minus {
      border: 1px solid #000;
      padding: 4px 5px;
      cursor: pointer;
   }
   span.plus {
      border: 1px solid #000;
      padding: 4px 5px;
      display: inline;
      cursor: pointer;
      vertical-align: baseline;
   }
</style>
<div class="cart">
   <section class="dekstop_view">
      <div class="container">
         <form method="post" action="{{ route('guest.confirm') }}" id="guestConfirm">
            @csrf
         <div class="row">
            <div class="col-8">
               @foreach($carts as $cart)
               <div class="row">
                  <div class="col-3">
                     <input type="hidden" id="cart_id{{ $cart->id }}" name="cartid[]" value="{{ $cart->id }}" >
                     <input type="hidden" id="mainprice{{ $cart->id }}" value="{{ $cart->mainPrice }}">
                     <img src="{{ $cart->product_image  }}" class="img-fluid">
                  </div>
                  <div class="col-6">
                     <p><b>{{ $cart->product_name }}</b></p>
                     <p class="m-0">{{ $cart->product_color }}</p>
                     <p>{{ $cart->product_size }}</p>
                     <div class="num mr-3">
                        <div class="number">
                           <span class="minus removeqnt" data-cartid="{{ $cart->id }}">-</span>
                              <input type="text" value="{{ $cart->quantity }}" id="qnt_{{ $cart->id }}"/>
                           <span class="plus addqnt" data-cartid="{{ $cart->id }}">+</span>
                        </div>
                     </div>
                     <div class="trash" data-cartid="{{ $cart->id }}">
                        <i class="fa fa-trash-alt"></i>
                     </div>
                  </div>
                  <div class="col-3">
                     <div class="orange mt-3">
                        <p > $<span id="product_price{{$cart->id}}">{{ $cart->product_price }}</span> MXN</p>
                     </div>
                  </div>
               </div>
              @endforeach
            </div>
            <div class="col-4">
               <p>Verification del pedido</p>
               <div class="form">
                  <div class="form-group ">
                     <p>Tiene un codigo promocional? Introduzcalo aqui:</p>
                     <div class="row">
                        <div class="col-8">
                           <input type="email" class="form-control mb-3" placeholder="Coupon or Voucher">
                        </div>
                        <div class="col-4 ">
                           <button class="save">Aplicar</button>
                        </div>
                     </div>

                     <div class="row bg">
                        <div class="col-6">
                           <p>Total</p>
                           <input type="hidden" name="total" id="realtotal" value="{{ $subtotal->price + 50 }}">
                           <p><b>$<span id="total">{{ $subtotal->price + 50 }}</span> </b><small>MXN</small></p>
                        </div>
                        <div class="col-6 text-right continue">
                           <p><span class="submitForm">Continuar</span></p>
                        </div>
                     </div>
                     <div class="row my-3">
                        <div class="col-12 text-center">
                           <p>Al continuar acepto <span class="orange_txt">Terminos y condiciones</span>de uso y <span class="orange_txt">Politica de privacidad</span></p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </form>
      </div>
   </section>
</div>
<!-- dekstop_view  -->

<!-- mobile_view  -->
<div class="mobile_view">
   <header class="header header-transparent">
      <div class="header-middle">
         <div class="container">
            <div class="header-left">
               <a href="details.html"><i class='fas fa-close ml-2' style="font-size: 20px;"></i></a>&nbsp;&nbsp;

            </div>
            <!-- End .header-left -->
            <a href="#" class="logo">
               <b><span class="font">Carrito</span></b>
            </a>
            <!-- End .header-right -->
         </div>
         <!-- End .container-fluid -->
      </div>
      <!-- End .header-middle -->

   </header>
   <form method="post" action="{{ route('guest.confirm') }}" id="guestConfirm">
      @csrf
   <div class="container-fluid mt-62">
      @foreach($carts as $cart)
      <div class="row ">
         <div class="col-12 mt-4">
            <i class="fas fa-ellipsis-v dot"></i>
            <div class="box">
               <input type="hidden" id="cart_id{{ $cart->id }}" name="cartid[]" value="{{ $cart->id }}" >
               <input type="hidden" id="mainprice{{ $cart->id }}" value="{{ $cart->mainPrice }}">
               <img src="{{ $cart->product_image  }}" width="100%">
            </div>
            <p>$<span id="product_price{{$cart->id}}">{{ $cart->product_price }}</span> <small>MXN</small></p>
            <p class="p-0 m-0"><b>{{ $cart->product_name }}</b></p>
            <p>{{ $cart->product_color }}</p>
         </div>
      </div>
      @endforeach
      <div class="line_grey mt-3"></div>
      <div class="row py-2">
         <div class="col-8">
            <div class="form-group m-0">
               <input type="email" class="form-control" placeholder="Coupon or Voucher">
            </div>
         </div>
         <div class="col-4 text-center d-flex justify-content-center">
            <a href="" class="send"><i class="fas fa-arrow-right"></i></a>
         </div>
      </div>
      <div class="line_grey"></div>

      <div class="row mt-3">
         <div class="col-6">
            <div class="pull-left">
               <p>Subtotal:</p>
            </div>
         </div>
         <div class="col-6">
            <div class="pull-right">
               <p><span id="product_price{{$cart->id}}">${{ $subtotal->price }}</span> <small>mxn</small></p>
            </div>
         </div>

      </div>
      <div class="row">
         <div class="col-6">
            <div class="pull-left">
               <p>Envio:</p>
            </div>
         </div>
         <div class="col-6">
            <div class="pull-right">
               <p>$50 <small>mxn</small></p>
            </div>
         </div>
      </div>
      <hr>
      <div class="row ">
         <div class="col-6 text-center">
            <p >Total:</p>
         </div>
         <div class="col-6 text-right">
            <p class="or"><b>${{ $subtotal->price + 50 }} <small>mxn</small></b></p>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <h4 class="text-center"><b>Also you can like this products</b></h4>
         </div>
      </div>

      <div class="row">
         @foreach($products as $product)
            <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
         <div class="col-6 col-md-4 col-lg-3 col-xl-2">
            <div class="product-default">
               <figure>
                  <a href="{{ route('product.description',$productId) }}">
                     <img src="{{URL::asset('storage/images/'. $product->images) }}">
                  </a>
               </figure>
               <div class="product-details">
                  <h2 class="product-title">
                     <a href="#">{{ $product->pname }}</a>
                  </h2>
                  <div class="price-box">
                     <span class="product-price"><i>${{ $product->product_price }}</i> MXN</span>
                  </div>
                  <!-- End .price-box -->
                  <div class="category-wrap">
                     <div class="category-list">
                        <a href="#" class="product-category">recibelo</a>
                     </div>
                  </div>
               </div>
               <!-- End .product-details -->
            </div>
         </div>
            @endforeach
      </div>
   </div>
   </form>
   <div class="product-action mt-3">
      <div class="product_price ml-3">
         <span>Total</span>
         <input type="hidden" name="total" id="realtotal" value="{{ $subtotal->price + 50 }}">
         <span><b>${{ $subtotal->price + 50 }}Mxn</b></span>
      </div>
      <a class="paction add-compare" title="Add to compare">
         <span class="submitForm">Continuar</span>
      </a>
   </div>
   <div class="container-fluid mt-3">
      <div class="row">

         <div class="col-6 col-md-4 col-lg-3 col-xl-2">
            <div class="product-default">
               <figure>
                  <a href="#">
                     <img src="assets/img/p-4.png">
                  </a>
               </figure>
               <div class="product-details">
                  <h2 class="product-title">
                     <a href="#">Descuentos exclusivos! Solo para app</a>
                  </h2>
                  <div class="price-box">
                     <span class="product-price"><i>$39</i> MXN</span>
                  </div>
                  <!-- End .price-box -->
                  <div class="category-wrap">
                     <div class="category-list">
                        <a href="#" class="product-category">recibelo</a>
                     </div>
                  </div>
               </div>
               <!-- End .product-details -->
            </div>
         </div>
         <div class="col-6 col-md-4 col-lg-3 col-xl-2">
            <div class="product-default">
               <figure>
                  <a href="#">
                     <img src="assets/img/p-3.png">
                  </a>
               </figure>
               <div class="product-details">
                  <h2 class="product-title">
                     <a href="#">Descuentos exclusivos! Solo para app</a>
                  </h2>
                  <div class="price-box">
                     <span class="product-price"><i>$39</i> MXN</span>
                  </div>
                  <!-- End .price-box -->
                  <div class="category-wrap">
                     <div class="category-list">
                        <a href="#" class="product-category">recibelo</a>
                     </div>
                  </div>
               </div>
               <!-- End .product-details -->
            </div>
         </div>
      </div>
   </div>
</div>
<!-- mobile_view  -->

<div class="mobile-menu-overlay"></div>
<!-- End .mobil-menu-overlay -->
<div class="mobile-menu-container">
   <div class="mobile-menu-wrapper">
      <div class="menu-top"> <span><img src="assets/img/logo.png"></span><span class="mobile-menu-close"><img src="assets/img/close.png"></span></div>
      <nav class="mobile-nav">
         <ul class="mobile-menu">
            <li class="active"><a href="listing.html">Categorías</a></li>
            <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="caret">Más vendido</span></a>
               <ul class="dropdown-menu">
                  <li><a href="listing.html">submenu</a></li>
                  <li><a href="listing.html">submenu</a></li>
                  <li><a href="listing.html">submenu</a></li>
               </ul>
            </li>
            <li>
               <a href="listing.html">Liquidacíon</a>
            </li>
            <li>
               <a href="listing.html">Contactanos</a>
            </li>
         </ul>
      </nav>
      <!-- End .mobile-nav -->
   </div>
   <!-- End .mobile-menu-wrapper -->
</div>
<!-- End .mobile-menu-container -->

<!-- footer  -->
<footer>
   <div class="container">
      <div class="row">
         <div class="col-lg-6 col-12">
            <p>Rasterio de Predidos y dudas sobre nuestros articulos</p>
            <p>Mensaje de WhatsApp: 55 8732<br>2760</p>
         </div>
         <div class="col-lg-6 col-12">
            <img src="assets/img/app-store.png" width="150px">
            <img src="assets/img/google.png" width="150px">
         </div>
      </div>
      <div class="row">
         <div class="col-lg-6"></div>
         <div class="col-lg-6">
            <p>Menu Inferior</p>
            <ul>
               <li><a href="#">Busquesda</a></li>
               <li><a href="#">Preguntas Frecuentes</a></li>
               <li><a href="#">Politica de privacidad</a></li>
               <li><a href="#">Envio Y devoluciones</a></li>
               <li><a href="#">Quiesnes somos</a></li>
            </ul>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <p>Atencion Al Cliente (Cambios)</p>
            <p>Mensaje de WhatsApp: 55 8029<br>8963</p>
         </div>
      </div>
   </div>
</footer>
<!-- footer  -->



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="{{ asset('assets/js/number.js') }}"></script>
<script src="{{ asset('assets/js/toastr.min.js') }}"></script>
<script>
   $('.removeqnt').off('click').on('click',function () {

      var _that = $(this);
      var cartId = _that.attr('data-cartid');
      var productPrice = $('#product_price'+cartId).text();
      var mainprice = $('#mainprice'+cartId).val();
      var qunty = $('#qnt_'+cartId).val();
      if(qunty > 1) {
         qunty = parseInt(qunty) - 1;
         var subtotal = parseInt(qunty) * parseInt(mainprice);
         $('#product_price' + cartId).text(subtotal);
         var total = $('#total').text();
         var mainTotal = parseInt(total) - parseInt(mainprice);
         $('#total').text(mainTotal);
         $('#realtotal').text(mainTotal);

      }
      ajaxCartUpdate(cartId,qunty,subtotal);

   })

   $('.addqnt').off('click').on('click',function () {

      var _that = $(this);
      var cartId = _that.attr('data-cartid');
      var productPrice = $('#product_price'+cartId).text();
      var mainprice = $('#mainprice'+cartId).val();
      var qunty = $('#qnt_'+cartId).val();
         qunty = parseInt(qunty) + 1;
      var subtotal = parseInt(qunty) * parseInt(mainprice);
      $('#product_price'+cartId).text(subtotal);
      var total = $('#total').text();
     var mainTotal = parseInt(total) + parseInt(mainprice);
      $('#total').text(mainTotal);
      $('#realtotal').text(mainTotal);
      ajaxCartUpdate(cartId,qunty,subtotal);

   })

   function ajaxCartUpdate(cartId,qunty,subtotal) {
      $.ajax(
              {
                 url: '/update_cart',
                 type: 'POST',
                 data: {
                    _token: "{{ csrf_token() }}",
                    "cartid": cartId,
                    "qunty": qunty,
                    "subtotal": subtotal,
                 },
                 success: function (result) {
                    if (result.success) {
                       return true
                       // toastr.success('Cart updated successfully');
                    } else {
                       return false;
                    }
                 }
              });
   }

   $('.trash').off('click').on('click',function () {
      var _that = $(this);
      var cartId = _that.attr('data-cartid');
      if (!confirm("Do you want to remove this cart ?")){
         return false;
      }
      else{
         $.ajax(
                 {
                    url: '/remove_cart',
                    type: 'POST',
                    data: {
                       _token: "{{ csrf_token() }}",
                       "cartid": cartId,
                    },
                    success: function (result) {
                       if (result.success) {
                          window.location.reload();
                          // toastr.success('Cart updated successfully');
                       } else {
                          return false;
                       }
                    }
                 });
      }

   })

$('.submitForm').off('click').on('click',function () {
   $('form#guestConfirm').submit();

})
</script>
</body>
</html>