<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <script src='https://kit.fontawesome.com/a076d05399.js'></script>
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
      <title>checkout</title>
   </head>
   <body class="checkout">
       <!-- mobile view  -->
       <div class="mobile_view">
            <header class="header header-transparent">
                <div class="header-middle">
                <div class="container-fluid">
                    <div class="header-left">
                    <i class="fas fa-arrow-left" style="color:#FF7100;font-size: 20px;"></i>&nbsp;&nbsp;
                        <a href="{{ url('/') }}"><img src="{{ asset('assets/img/logo.png') }}" width="150px"></a>

                    </div>
                    <!-- End .header-left -->

                    <!-- End .header-right -->
                </div>
                <!-- End .container-fluid-fluid -->
                </div>
                <!-- End .header-middle -->

            </header>
            <section class="check mt-62">
            <div class="container-fluid">
                <div class="row ">
                    <div class="col-8 ">
                        <img src="{{ asset('assets/img/CHECKOUT-PAGE.png') }}" width="30px">
                        <p class="price">Mostrar resumen del pedido <i class="fa fa-angle-down"></i></p>
                    </div>
                    <div class="col-4">
                        <p>$ {{ $total }} MVN</p>
                    </div>
                </div>

                </div>
            </section>


            <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Carrito</a></li>
                <li class="breadcrumb-item"><a href="#">Informacion</a></li>
                <li class="breadcrumb-item"><a href="#">Envios</a></li>
                <li class="breadcrumb-item active" aria-current="page">Pago</li>
            </ol>
            </nav>
           <form method="post" action="{{ route('update.confirm') }}" id="order_confirm">
               @csrf
            <section class="list">

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-8">
                                <div class="con">
                                    <p>Contacto</p>
                                    <p>{{ $guestInfo->email }}</p>
                                </div>
                                </div>
                                <div class="col-4">
                                    <div class="cam">
                                        <p class="or">Cambiar</p>
                                    </div>
                                </div>
                                <div class="line"></div>
                            </div>
                            <div class="row my-3">
                                <div class="col-8">
                                <div class="con">
                                    <p>Enviar a</p>
                                    <p>{{ $guestInfo->reference }}, {{ $guestInfo->street }},{{ $guestInfo->street_number }} {{ $guestInfo->postal_code }}, {{ $guestInfo->state }}</p>
                                </div>
                                </div>
                                <div class="col-4">
                                    <div class="cam">
                                        <p class="or">Cambiar</p>
                                    </div>
                                </div>
                                <div class="line"></div>
                            </div>
                            <div class="row my-3">
                                <div class="col-8">
                                <div class="con">
                                    <p>Metodo</p>
                                    <p>{{ $shipping->title }} ${{ $shipping->charge }} MXN </p>
                                </div>
                                </div>
                                <div class="col-4">
                                    <div class="cam">
                                        <p></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <section>
                <div class="container-fluid">
                <h5>Descuento</h5>
                <div class="row py-2">

                    <div class="col-8">
                        <div class="form-group m-0">
                            <input type="email" class="form-control" placeholder="Codigo de descuento">

                            </div>
                    </div>
                    <div class="col-4 text-center d-flex justify-content-center">
                        <a href="javascript:history.back()"><i class="fas fa-arrow-right"></i></a>
                    </div>
                    </div>
                </div>
            </section>

            <section class="mt-5 pago">
            <div class="container-fluid">
                <h5>Pago</h5>
                <p>Todas las transacciones son seguras y estan encriptadas.</p>
                <div class="list">
                    <div class="row">
                    <div class="col-12">
                        <div class="row my-3">
                            <div class="col-12">
                                <div class="radios">
                                    <div class="radio">
                                        <input type="radio" id="radio1"  name="pay_by" checked value="card">
                                        <label for="radio1">
                                        <div class="checker1"></div>
                                        <b>{{ $paymentDetails->online_pay_title }}</b>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="row">
                            <div class="col-12 grey">
                                <div class="text-center">
                                    <?php echo $paymentDetails->online_pay_description;?>
                                </div>
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="row my-3">
                            <div class="col-12">
                                <div class="radios">
                                    <div class="radio">
                                        <input type="radio" id="radio2" name="pay_by" value="cash">
                                        <label for="radio2">
                                        <div class="checker"></div>
                                            <b>{{ $paymentDetails->cod_title }}</b>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                </div>

            </div>
            </section>
            <div class="container-fluid">
                <div class="row my-3">
                    <div class="col-12">
                        <button type="submit" class="btn-orange btn-block submitForm">Finalizar el pedido</button>
                    </div>
                </div>
            </div>
           </form>
       </div>
       <!-- mobile view  -->



       <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
       <script src="{{ asset('assets/js/popper.min.js') }}"></script>
       <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
       <script src="{{ asset('assets/js/number.js') }}"></script>
   </body>
</html>
