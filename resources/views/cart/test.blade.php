<p><b>SIZE:&nbsp;</b>
    <?php $k=0; ?>
    @foreach($colorSizes as $colorSize)
        <span class="red sizedata {{ $k ==0 ?'active':''  }}">{{ $colorSize->size }}</span>&nbsp;
        <?php $k++; ?>
    @endforeach
</p>

<script>
    $('.sizedata').off('click').on('click',function () {
        var _that = $(this);
        $(".sizedata").removeClass("active");
        $(this).addClass("active");
    })
</script>
