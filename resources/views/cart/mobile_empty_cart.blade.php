<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <script src='https://kit.fontawesome.com/a076d05399.js'></script>
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
      <title>Empty_cart</title>
       <style>
           .list .sele1 {
               width: 78px;
           }


           .list li {
               cursor: pointer;
               position: relative;
           }

           .header-right .list li img {
               width: 40px;
               margin-top: -5px;
               display: inherit;
               margin: 0 auto;
           }

           .counters_new {
               position: absolute;
               top: 0px;
               left: 38px;
               background: #fff;
               border: 1px solid #e16b1a;
               color: #e16b1a;
               border-radius: 50%;
               padding: 0px 5px;
           }
           .inherit {
               display: inherit;
               margin: 0 auto;
               width: 50px;
           }
       </style>
   </head>
   <body class="empty">

      <div class="mobile_view">

         <!-- For mobile -->

          <header class="header header-transparent">
              <div class="header-middle">
                  <div class="container-fluid">
                      <div class="row">
                          <div class="col-lg-4 col-4">
                              <div class="header-left" style="float: left;margin-top: 14px;">
                                  <button class="mobile-menu-toggler" type="button">
                                      <img src="{{asset('assets/img/mob-menu.png')}}">
                                  </button>

                              </div>
                              <div class="d-lg-none d-block">
                                  <div class="header-right">
                                      <ul class="list">
                                          <li><img src="{{ asset('assets/img/Iconos y Banner-08.jpg') }}" class="inherit">
                                              <select class="currency-selector sele1 branches">
                                                  <option selected>CDMX</option>
                                                  <option >ABC</option>
                                              </select>
                                          </li>
                                      </ul>
                                  </div>
                              </div>

                          </div>
                          <div class="col-lg-4 col-4">
                              <a href="{{ url('/') }}" class="logo">
                                  <img src="{{ asset('assets/img/logo.png') }}" alt="Logo">
                              </a>
                          </div>
                          <div class="col-lg-4 col-12 d-lg-block d-none">
                              <div class="header-right">
                                  <ul class="list">
                                      <li><img src="{{ asset('assets/img/Iconos y Banner-08.jpg') }}" class="inherit">
                                          <select class="currency-selector sele1 branches">
                                              <option selected>CDMX</option>
                                              <option >ABC</option>
                                          </select>
                                      </li>
                                      <li>
                                          <img src="{{ asset('assets/img/mobi.gif') }}" class="inherit">
                                          <select class="currency-selector sele2">
                                              <option selected>App</option>
                                              <option >ABC</option>
                                              <option >Management</option>
                                              <option >Business</option>
                                              <option >Gangabox</option>
                                          </select>
                                      </li>
                                      <li>
                                          <div id="counter" class="counters_new">
                                              <span class="count totalcart">{{ $carttotal->cartcount }}</span>
                                          </div>
                                          <a href="{{ route('cart.list') }}">    <img src="{{ asset('assets/img/2 icon gray.png') }}" ><p style="font-size: 17px;font-weight: bold;color: #949399;">Carrito</p></a>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                          <div class="col-lg-4 col-4 d-lg-none d-block">
                              <div class="header-right">
                                  <ul class="list">
                                      <li>
                                          <div id="counter" class="counters">
                                              <span class="count totalcart">0</span>
                                          </div>
                                          <a href="{{ route('cart.list') }}">  <img src="{{ asset('assets/img/2 icon gray.png') }}" ><p style="font-size: 17px;font-weight: bold;color: #949399;">Carrito</p></a></li>
                                  </ul>
                              </div>
                          </div>
                      </div>

                      <!-- End .header-left -->


                      <!-- End .header-right -->
                  </div>
                  <!-- End .container-fluid -->
              </div>
              <!-- End .header-middle -->
              <div class="top-cate">

              </div>
          </header>
         <!-- End .header -->
         <!-- end -->
         <!-- banner -->
         <div class="banner text-center mt-62">
             <img src="{{ asset('assets/img/empty.png') }}" class="img-fluid">
             <p>El carrito esta vacio</p>
             <a href="{{ route('product.list') }}" class="btn">ve a buscar algo que te guste.</a>
             <p class="mt-4">Supongo que te gusta</p>
         </div>
         <!-- banner -->
      </div>




      <section class="container-fluid item-product">
         <div class="row row-sm">
             @foreach($products as $product)
                 <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
            <div class="col-6 ">
                <div class="product-default">
                <figure>
                    <a href="{{ route('product.description',$productId) }}">
                        @if(env('APP_ENV') == 'local')

                            <img src="{{env('localUrl').'storage/images/'. $product->images }}">
                        @else
                            <img src="{{env('serverUrl').'storage/images/'. $product->images }}">
                        @endif
                    </a>
                </figure>
                <div class="product-details">
                    <h2 class="product-title">
                        <a href="{{ route('product.description',$productId) }}">{{ $product->pname }}</a>
                    </h2>
                    <div class="price-box">
                        <span class="product-price"><i>${{ $product->product_price }}</i> MXN</span>
                    </div>
                    <!-- End .price-box -->
                    <div class="category-wrap">
                        <div class="category-list">
                            <a href="#" class="product-category">recibelo</a>
                        </div>
                    </div>
                </div>
                <!-- End .product-details -->
                </div>
            </div>
                 @endforeach



         </div>
      </section>
      <div class="mobile-menu-overlay"></div>
      <!-- End .mobil-menu-overlay -->
      <div class="mobile-menu-container">
         <div class="mobile-menu-wrapper">
            <div class="menu-top"> <span><img src="assets/img/logo.png"></span><span class="mobile-menu-close"><img src="assets/img/close.png"></span></div>
            <nav class="mobile-nav" id="submenu">
               <ul class="mobile-menu">
                  <li class="active"><a href="listing.html">Categorías</a></li>
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="caret">Más vendido</span></a>
                     <ul class="dropdown-menu">
                        <li><a href="listing.html">submenu</a></li>
                        <li><a href="listing.html">submenu</a></li>
                        <li><a href="listing.html">submenu</a></li>
                     </ul>
                  </li>
                  <li>
                     <a href="listing.html">Liquidacíon</a>
                  </li>
                  <li>
                     <a href="listing.html">Contactanos</a>
                  </li>
               </ul>
            </nav>
            <!-- End .mobile-nav -->
         </div>
         <!-- End .mobile-menu-wrapper -->
      </div>
      <!-- End .mobile-menu-container -->

      <!-- footer  -->
      <footer>
         <div class="container">
            <div class="row">
               <div class="col-lg-6 col-12">
                  <p>Rasterio de Predidos y dudas sobre nuestros articulos</p>
                  <p>Mensaje de WhatsApp: 55 8732<br>2760</p>
               </div>
               <div class="col-lg-6 col-12">
                  <img src="assets/img/app-store.png" width="150px">
                  <img src="assets/img/google.png" width="150px">
               </div>
            </div>
            <div class="row">
               <div class="col-lg-6"></div>
               <div class="col-lg-6">
                  <p>Menu Inferior</p>
                  <ul>
                     <li><a href="#">Busquesda</a></li>
                     <li><a href="#">Preguntas Frecuentes</a></li>
                     <li><a href="#">Politica de privacidad</a></li>
                     <li><a href="#">Envio Y devoluciones</a></li>
                     <li><a href="#">Quiesnes somos</a></li>
                  </ul>
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <p>Atencion Al Cliente (Cambios)</p>
                  <p>Mensaje de WhatsApp: 55 8029<br>8963</p>
               </div>
            </div>
         </div>
      </footer>
      <!-- footer  -->

      <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
      <script src="{{ asset('assets/js/popper.min.js') }}"></script>
      <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
{{--      <script src="{{ asset('assets/js/number.js') }}"></script>--}}
      <script>
          updateCart();
         $('.mobile-menu-toggler').on('click', function (e) {
         $('body').toggleClass('mmenu-active');
         $(this).toggleClass('active');
         e.preventDefault();
         });

         $('.mobile-menu-overlay, .mobile-menu-close').on('click', function (e) {
         $('body').removeClass('mmenu-active');
         e.preventDefault();
         });

         function updateCart() {
             $.ajax(
                 {
                     url: ' {{ route('get.details') }} ',
                     type: 'get',
                     success: function (result) {
                         if (result.success) {
                             $('.branches').html('');
                             $('.count').text(result.cartTotal.cartcount);
                             // var mapData = JSON.parse(result.data);
                             $('.branches').html(result.data);
                         } else {
                             return false;
                         }
                     }
                 });
         }
      </script>
      <script>
          getFamilySub();
          function getFamilySub()
          {
              $.ajax(
                  {
                      url: ' {{ route('get.family.sub') }} ',
                      type: 'get',
                      success: function (result) {
                          if (result.success) {
                              $('#submenu').html('');
                              // $('.count').text(result.cartTotal.cartcount);
                              // var mapData = JSON.parse(result.data);
                              $('#submenu').html(result.data);
                          } else {
                              return false;
                          }
                      }
                  });
          }
      </script>
   </body>
</html>
