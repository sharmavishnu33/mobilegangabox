<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src='https://kit.fontawesome.com/a076d05399.js'></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <link rel="stylesheet" href="{{ asset('css/style1.css') }}">
      <title>checkout</title>
       <style>
           .scroll {
               max-height: 350px;
               height: 300px;
               overflow-x: hidden;
               overflow-y: scroll;
           }
       </style>
   </head>
   <body class="checkout">
       <!-- dekstop-view  -->
       <form method="post" action="{{ route('order.confirm') }}" id="order_confirm">
           @csrf
       <div class="dekstop_view">
        <section class="greys"></section>
           <div class="container-fluid">
               <div class="row mt-3">
                   <div class="col-6">
                       <div class="row">
                           <div class="col-lg-2"><i class="fa fa-close mr-5"></i></div>
                           <div class="col-lg-8">
                                <div class="header">
                                    <img src="assets/img/logo.png "width="150px">
                                </div>
{{--                                <form class="form  mt-3">--}}

                                    <div class="row">
                                        <div class="col-12">
                                            <label for="exampleFormControlSelect1">Ingrese su email</label>
                                            <div class="input-group mb-2 mr-sm-2">
                                                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Ingrese su email" name="email" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <label for="exampleFormControlSelect1">Ingrese su nombore y apellido</label>
                                            <div class="input-group mb-2 mr-sm-2">
                                                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Ingrese su nombore y apellido" name="fullname" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Ingrese su el Estado o Provincia</label>
                                                <select class="form-control" id="exampleFormControlSelect1" name="state" required>
                                                    <option> Select Estado</option>
                                                    <option value="Estado de Mexico">Estado de Mexico</option>
                                                    <option value="CDMX">CDMX</option>

                                                </select>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                        <p>Ingrese con su numero de celular</p>
                                        <!-- End .product-single-tabs -->
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="MXN+52" name="mobile_code" required>
                                        </div>
                                        <div class="col-8">

                                            <div class="input-group mb-2 mr-sm-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fas fa-mobile-alt"></i>
                                                </div>
                                            </div>
                                            <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Ingrese su numero" name="mobile_number" required>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <p>Ingrese su codigo postal</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="input-group mb-2 mr-sm-2">
                                                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Ingrese su codigo postal" name="postal_code" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <p>Calle</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="input-group mb-2 mr-sm-2">
                                                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Ingrese la direccion de entrega" name="street" required>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-12">
                                            <p>Numero exterior</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="input-group mb-2 mr-sm-2">
                                                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Ingrese la numero exterior" name="street_number" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <p>Referencia</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="input-group mb-2 mr-sm-2">
                                                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Ingrese alguna referencia de la direccion" name="reference" required>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row mt-2">
                                        <div class="col-12">
                                            <h5>Pago</h5>
                                            <p>Todas las transacciones son seguras y estan encriptadas.</p>
                                            <div class="list">
                                                <div class="row">
                                                <div class="col-12">
                                                    <div class="row my-3">
                                                        <div class="col-12">
                                                            <div class="radios">
                                                                <div class="radio">
                                                                    <input type="radio" id="radio1" name="pay_by" checked value="card">
                                                                    <label for="radio1">
                                                                    <div class="checker1"></div>
                                                                    <b>Paga con tarijeta al momento de recibir tu pedido!</b>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="line"></div>
                                                    <div class="row">
                                                        <div class="col-12 grey">
                                                            <div class="text-center">
                                                                <p>Paga con tarjeta de credito o debito al momento de recibir tu pedido!</p>
                                                                <p>*NO SE ACEPTAN CANCELACIONES, EN CASO DE NO SER</p>
                                                                <p>RECIBIDO NO PODRAS REALIZAR OTRO</p>
                                                                <p>PEDIDO DESDE LA PLATAFORMA</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="line"></div>
                                                    <div class="row my-3">
                                                        <div class="col-12">
                                                            <div class="radios">
                                                                <div class="radio">
                                                                <input type="radio" id="radio2" name="pay_by" value="cash">
                                                                    <label for="radio2">
                                                                    <div class="checker"></div>
                                                                    <b>Pagalo en efectivo al momento de recibir!</b>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>

{{--                                </form>--}}
                           </div>
                       </div>
                    </div>

                   <div class="col-6 ">
                       <div class="fixed">
                           <div class="scroll">
                               @foreach($carts as $cart)
                                   <div class="row">
                                       <div class="col-3">
                                           <input type="hidden" name="cartId[]" value="{{ $cart->id }}">
                                           <img src="{{ $cart->product_image  }}" class="img-fluid">
                                       </div>
                                       <div class="col-5">
                                           <p><b>{{ $cart->product_name }}</b></p>
                                           <p class="m-0">{{ $cart->product_color }}</p>
                                           <p>{{ $cart->product_size }}</p>
                                       </div>
                                       <div class="col-4">
                                           <div class="orange mt-3">
                                               <p class="mr-3">X{{ $cart->quantity }}</p>
                                               <p>${{ $cart->product_price }} MXN</p>
                                           </div>
                                       </div>
                                   </div>
                               @endforeach

                           </div>
                           <div class="row mt-3 ">
                               <div class="col-8">
                                   <p class="m-0">Subtotal</p>
                                   <p class="m-0">Envio</p>
                                   <p>(Recibelo Manana)</p>
                               </div>
                               <div class="col-4 ">
                                   <p>${{ $subtotal->price }} MXN</p>
                                   <p>$50 MXN</p>
                               </div>
                           </div>
                           <div class="row ">
                               <div class="col-8">
                                   <p>Total</p>
                               </div>
                               <div class="col-4">
                                   <input type="hidden" name="total" value="{{ $total }}">
                                   <p>${{ $total}} MXN</p>
                               </div>
                           </div>
                           <div class="row my-5">
                               <div class="col-12">
                                   <button type="submit" class="btn btn-block submitForm">Finalizer Pedido</button>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>

       </div>
       </form>
       <!-- dekstop-view  -->


       <!-- mobile view  -->
       <div class="mobile_view">
            <header class="header header-transparent">
                <div class="header-middle">
                <div class="container">
                    <div class="header-left">
                    <i class="fas fa-arrow-left" style="color:#FF7100;font-size: 20px;"></i>&nbsp;&nbsp;
                        <a href="details.html"><img src="assets/img/logo.png" width="150px"></a>

                    </div>
                    <!-- End .header-left -->

                    <!-- End .header-right -->
                </div>
                <!-- End .container-fluid -->
                </div>
                <!-- End .header-middle -->

            </header>
            <section class="check mt-62">
            <div class="container-fluid ">
                <div class="row ">
                    <div class="col-8 ">
                        <img src="assets/img/CHECKOUT-PAGE.png" width="30px">
                        <p class="price">Mostrar resumen del pedido <i class="fa fa-angle-down"></i></p>
                    </div>
                    <div class="col-4">
                        <p>$ 249.00 MVN</p>
                    </div>
                </div>

                </div>
            </section>


            <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Carrito</a></li>
                <li class="breadcrumb-item"><a href="#">Informacion</a></li>
                <li class="breadcrumb-item"><a href="#">Envios</a></li>
                <li class="breadcrumb-item active" aria-current="page">Pago</li>
            </ol>
            </nav>

            <section class="list">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-8">
                                <div class="con">
                                    <p>Contacto</p>
                                    <p>edysalame@gmail.com</p>
                                </div>
                                </div>
                                <div class="col-4">
                                    <div class="cam">
                                        <p class="or">Cambiar</p>
                                    </div>
                                </div>
                                <div class="line"></div>
                            </div>
                            <div class="row my-3">
                                <div class="col-8">
                                <div class="con">
                                    <p>Enviar a</p>
                                    <p>Al lado, Parque de andalucia 2 casca c, Casca c 52786 Huixquilucan estado de Mexico Df, Mexico</p>
                                </div>
                                </div>
                                <div class="col-4">
                                    <div class="cam">
                                        <p class="or">Cambiar</p>
                                    </div>
                                </div>
                                <div class="line"></div>
                            </div>
                            <div class="row my-3">
                                <div class="col-8">
                                <div class="con">
                                    <p>Metodo</p>
                                    <p>Entrega Para EL SABADO en el Transcurso del dia $50.00 MXN </p>
                                </div>
                                </div>
                                <div class="col-4">
                                    <div class="cam">
                                        <p></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <section>
                <div class="container">
                <h5>Descuento</h5>
                <div class="row py-2">

                    <div class="col-8">
                        <div class="form-group m-0">
                            <input type="email" class="form-control" placeholder="Codigo de descuento">
                            <!-- <select class="form-control" id="exampleFormControlSelect1">
                                <option>Coupon or voucher</option>
                                <option>Coupon or voucher</option>
                                <option>Coupon or voucher</option>
                                <option>Vouchers</option>
                                <option>Vouchers</option>
                            </select> -->
                            </div>
                    </div>
                    <div class="col-4 text-center d-flex justify-content-center">
                        <a href="" class="send"><i class="fas fa-arrow-right"></i></a>
                    </div>
                    </div>
                </div>
            </section>

            <section class="mt-5 pago">
            <div class="container">
                <h5>Pago</h5>
                <p>Todas las transacciones son seguras y estan encriptadas.</p>
                <div class="list">
                    <div class="row">
                    <div class="col-12">
                        <div class="row my-3">
                            <div class="col-12">
                                <div class="radios">
                                    <div class="radio">
                                        <input type="radio" id="radio1" name="radio" checked>
                                        <label for="radio1">
                                        <div class="checker1"></div>
                                        <b>Paga con tarijeta al momento de recibir tu pedido!</b>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="row">
                            <div class="col-12 grey">
                                <div class="text-center">
                                    <p>Paga con tarjeta de credito o debito al momento de recibir tu pedido!</p>
                                    <p>*NO SE ACEPTAN CANCELACIONES, EN CASO DE</p>
                                    <p>*NO SER RECIBIDO NO PODRAS REALIZAR OTRO</p>
                                    <p>PEDIDO DESDE LA PLATAFORMA</p>
                                </div>
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="row my-3">
                            <div class="col-12">
                                <div class="radios">
                                    <div class="radio">
                                        <input type="radio" id="radio2" name="radio">
                                        <label for="radio2">
                                        <div class="checker"></div>
                                        <b>Pagalo en efectivo al momento de recibir!</b>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                </div>

            </div>
            </section>
            <div class="container">
                <div class="row my-3">
                    <div class="col-12">
                        <button class="btn-orange btn-block">Finalizar el pedido</button>
                    </div>
                </div>
            </div>
       </div>
       <!-- mobile view  -->



      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
      <script src="assets/js/number.js"></script>
   <script>
       // $('.submitForm').off('click').on('click',function () {
       //     $('form#order_confirm').submit();
       //
       // })
   </script>
   </body>
</html>
