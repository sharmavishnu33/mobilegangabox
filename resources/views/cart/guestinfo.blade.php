<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <script src='https://kit.fontawesome.com/a076d05399.js'></script>
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
      <title>Document</title>
   </head>
   <body class="login">
      <header class="header header-transparent ">
         <div class="header-middle">
            <div class="container">
               <div class="header-left">
                  <button class="mobile-menu-toggler" type="button">
                  <!-- <img src="assets/img/mob-menu.png"> -->
                  </button>
                   <a href="javascript:history.back()">  <i class='fas fa-times ml-2' style="font-size: 20px;"></i>&nbsp;&nbsp;</a>

               </div>
               <!-- End .header-left -->
			       <a href="{{ url('/') }}" class="logo">
                  <img src="{{ asset('assets/img/logo.png') }}" alt="Logo">
                  </a>
               <!-- End .header-right -->
            </div>
            <!-- End .container-fluid -->
         </div>
         <!-- End .header-middle -->

      </header>
      <div class="container-fluid">

         <div class="row">
            <div class="col-12">
                <form class="form mt-62 " method="post" action="{{ route('order.confirm') }}" id="order_confirm">
                    @csrf

                    <div class="row">
                        <div class="col-12">
                            <p>Ingrese su email</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group mb-2 mr-sm-2">
                                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Ingrese su email" name="email" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <p>Ingrese su nombore y apellido</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group mb-2 mr-sm-2">
                                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Ingrese su nombore y apellido" name="fullname" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Ingrese su el Estado o Provincia</label>
                                <select class="form-control" id="exampleFormControlSelect1" name="state" required>
                                    <option> Select Estado</option>
                                    @foreach($state as $states)
                                    <option value="{{ $states->name }}">{{ $states->name }}</option>
                                    @endforeach

                                </select>
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                        <p>Ingrese con su numero de celular</p>
                        <!-- End .product-single-tabs -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="MXN+52" name="mobile_code" required>
                        </div>
                        <div class="col-8">

                            <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-mobile-alt"></i>
                                </div>
                            </div>
                                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Ingrese su numero" name="mobile_number" required>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-12">
                            <p>Ingrese su codigo postal</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group mb-2 mr-sm-2">
                                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Ingrese su codigo postal" name="postal_code" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <p>Calle</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group mb-2 mr-sm-2">
                                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Ingrese la direccion de entrega" name="street" required>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-12">
                            <p>Numero exterior</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group mb-2 mr-sm-2">
                                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Ingrese la numero exterior" name="street_number" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <p>Referencia</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group mb-2 mr-sm-2">
                                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Ingrese alguna referencia de la direccion" name="reference" required>
                            </div>
                        </div>
                    </div>
                    @foreach($carts as $cart)
                                <input type="hidden" name="cartId[]" value="{{ $cart->id }}">
                    @endforeach
                    <input type="hidden" name="total" value="{{ $total }}">
                    <div class="row my-3">
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-block submitForm">Siguiente</button>
                        </div>
                    </div>


                  </form>

            </div>
         </div>
      </div>

      <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
      <script src="{{ asset('assets/js/popper.min.js') }}"></script>
      <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('assets/js/number.js') }}"></script>
{{--      <script>--}}
{{--          $('.submitForm').off('click').on('click',function () {--}}
{{--              $('form#order_confirm').submit();--}}

{{--          })--}}
{{--      </script>--}}
   </body>
</html>
