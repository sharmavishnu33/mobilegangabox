@include('front_layouts.header')
<!-- For mobile -->
<header class="header header-transparent">
    <div class="header-middle">
        <div class="container">
            <div class="header-left">
                <button class="mobile-menu-toggler" type="button">
                    <img src="{{ asset('assets/img/mob-menu.png') }}">
                </button>
                <select class="currency-selector">
                    <option selected>CDMX</option>
                    <option >ABC</option>
                </select>
            </div>
            <!-- End .header-left -->
            <a href="{{ url('/') }}" class="logo">
                <img src="{{ asset('assets/img/logo.png') }}" alt="Logo">
            </a>
            <div class="header-right">
                <a href="login.html">
                    <div class="header-user">
                        <img src="{{ asset('assets/img/mob-user.png') }}">
                    </div>
                </a>
                <div class="dropdown cart-dropdown">
                    <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                        <img src="{{ asset('assets/img/mob-cart.png') }}">
                        <!--<span class="cart-count">2</span>-->
                    </a>
                </div>
                <!-- End .dropdown -->
                <div class="header-search">
                    <a href="#" class="search-toggle" role="button"><img src="{{ asset('assets/img/mob-search.png') }}"></a>
                    <!-- <form action="#" method="get">
                       <div class="header-search-wrapper">
                           <input type="search" class="form-control" name="q" id="q" placeholder="I'm searching for..." required="">

                           <button class="btn" type="submit"><i class="icon-search-3"></i></button>
                       </div>
                       </form>-->
                </div>
            </div>
            <!-- End .header-right -->
        </div>
        <!-- End .container-fluid -->
    </div>
    <!-- End .header-middle -->

</header>
<!-- End .header -->
<!-- end -->
<!-- banner -->
<div class="banner text-center mt-62">
    <img src="{{ asset('assets/img/empty.png') }}" class="img-fluid">
    <p>El carrito esta vacio</p>
    <a href="{{ route('product.list') }}" class="btn">ve a buscar algo que te guste.</a>
    <p class="mt-4">Supongo que te gusta</p>
</div>
<!-- banner -->

<section class="container-fluid item-product">
    <div class="row row-sm">
        <div class="col-6 col-md-4 col-lg-3 col-xl-2">
            <div class="product-default">
                <figure>
                    <a href="#">
                        <img src="{{ asset('assets/img/p-4.png') }}">
                    </a>
                </figure>
                <div class="product-details">
                    <h2 class="product-title">
                        <a href="#">Lampara Solar</a>
                    </h2>
                    <div class="price-box">
                        <span class="product-price"><i>MXN $399</i> </span>
                    </div>
                    <!-- End .price-box -->
                    <div class="category-wrap">
                        <div class="category-list">
                            <a href="#" class="product-category">recibelo</a>
                        </div>
                    </div>
                </div>
                <!-- End .product-details -->
            </div>
        </div>
        <div class="col-6 col-md-4 col-lg-3 col-xl-2">
            <div class="product-default">
                <figure>
                    <a href="#">
                        <img src="{{ asset('assets/img/p-3.png') }}">
                    </a>
                </figure>
                <div class="product-details">
                    <h2 class="product-title">
                        <a href="#">Reloj de Vestir Cuazo</a>
                    </h2>
                    <div class="price-box">
                        <span class="product-price"><i>MXN $399</i> </span>
                    </div>
                    <!-- End .price-box -->
                    <div class="category-wrap">
                        <div class="category-list">
                            <a href="#" class="product-category">recibelo</a>
                        </div>
                    </div>
                </div>
                <!-- End .product-details -->
            </div>
        </div>
        <div class="col-6 col-md-4 col-lg-3 col-xl-2">
            <div class="product-default">
                <figure>
                    <a href="#">
                        <img src="assets/img/p-4.png">
                    </a>
                </figure>
                <div class="product-details">
                    <h2 class="product-title">
                        <a href="#">Lampara Solar</a>
                    </h2>
                    <div class="price-box">
                        <span class="product-price"><i>MXN $399</i> </span>
                    </div>
                    <!-- End .price-box -->
                    <div class="category-wrap">
                        <div class="category-list">
                            <a href="#" class="product-category">recibelo</a>
                        </div>
                    </div>
                </div>
                <!-- End .product-details -->
            </div>
        </div>
        <div class="col-6 col-md-4 col-lg-3 col-xl-2">
            <div class="product-default">
                <figure>
                    <a href="#">
                        <img src="assets/img/p-3.png">
                    </a>
                </figure>
                <div class="product-details">
                    <h2 class="product-title">
                        <a href="#">Reloj de Vestir Cuazo</a>
                    </h2>
                    <div class="price-box">
                        <span class="product-price"><i>MXN $399</i> </span>
                    </div>
                    <!-- End .price-box -->
                    <div class="category-wrap">
                        <div class="category-list">
                            <a href="#" class="product-category">recibelo</a>
                        </div>
                    </div>
                </div>
                <!-- End .product-details -->
            </div>
        </div>


    </div>
</section>
<div class="mobile-menu-overlay"></div>
<!-- End .mobil-menu-overlay -->
<div class="mobile-menu-container">
    <div class="mobile-menu-wrapper">
        <div class="menu-top"> <span><img src="assets/img/logo.png"></span><span class="mobile-menu-close"><img src="assets/img/close.png"></span></div>
        <nav class="mobile-nav">
            <ul class="mobile-menu">
                <li class="active"><a href="listing.html">Categorías</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="caret">Más vendido</span></a>
                    <ul class="dropdown-menu">
                        <li><a href="listing.html">submenu</a></li>
                        <li><a href="listing.html">submenu</a></li>
                        <li><a href="listing.html">submenu</a></li>
                    </ul>
                </li>
                <li>
                    <a href="listing.html">Liquidacíon</a>
                </li>
                <li>
                    <a href="listing.html">Contactanos</a>
                </li>
            </ul>
        </nav>
        <!-- End .mobile-nav -->
    </div>
    <!-- End .mobile-menu-wrapper -->
</div>
<!-- End .mobile-menu-container -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script>
    $('.mobile-menu-toggler').on('click', function (e) {
        $('body').toggleClass('mmenu-active');
        $(this).toggleClass('active');
        e.preventDefault();
    });

    $('.mobile-menu-overlay, .mobile-menu-close').on('click', function (e) {
        $('body').removeClass('mmenu-active');
        e.preventDefault();
    });
</script>
</body>
</html>
