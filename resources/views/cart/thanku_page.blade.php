<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <script src='https://kit.fontawesome.com/a076d05399.js'></script>
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <link rel="stylesheet" href="{{ asset('css/style1.css') }}">
      <title>Gangabox | Thanku</title>
       <style>
           .scroll {
               max-height: 350px;
               height: 300px;
               overflow-x: hidden;
               overflow-y: scroll;
           }
       </style>
   </head>
   <body class="checkout">
       <!-- dekstop-view  -->
       <div class="dekstop_view">
        <section class="greys"></section>
           <div class="container-fluid">
               <div class="row mt-3">
                   <div class="col-6 pr-5">
                       <div class="row">
                           <div class="col-lg-8">
                                <div class="header">
                                    <img src="assets/img/logo.png "width="150px">
                                </div>
                           </div>
                       </div>
                       <div class="row my-3">
                           <div class="col-1">
                                <i class="far fa-check-circle fa-3x orange_txt"></i>
                           </div>
                           <div class="col-11">
                               <p class="m-0">order # {{ $orderNumber }}</p>
                               <p>Gracies "CLIENT!"</p>
                           </div>
                       </div>
                       <div class="row mb-3 thanku">
                           <div class="col-12 text-center">
                               <div class="box">
                                   <p><b>Tu Pedido esta confirmado</b></p>
                                   <p>te notificaremos cuando se envie</p>
                               </div>
                           </div>
                       </div>
                       <div class="row thanku">
                           <div class="col-12 ">
                               <div class="box">
                                   <p><b>Customer Information</b></p>
                                    <div class="row">
                                        <div class="col-6">
                                            <p>Shipping Address</p>
                                            <address>
                                                {{ $user->name }} <br>
                                                {{ $user->street_number }} {{ $user->street }}<br>
                                               {{ $user->state }} {{ $user->postal_code }}<br>
                                               {{ $user->mobile_code }} {{ $user->mobile }}
                                            </address>
                                            <p><b>Shipping Method</b></p>
                                            <p>Expedited Parcel</p>
                                        </div>
                                        <div class="col-6">
                                            <p>Shipping Address</p>
                                            <address>
                                                {{ $user->name }} <br>
                                                {{ $user->street_number }} {{ $user->street }}<br>
                                                {{ $user->state }} {{ $user->postal_code }}<br>
                                                {{ $user->mobile_code }} {{ $user->mobile }}
                                            </address>
                                            <p><b>Payment Method</b></p>
                                            <p>{{ $orderDetails->pay_by }}</p>
                                        </div>
                                    </div>
                               </div>
                           </div>
                       </div>
                       <div class="row mt-5">
                           <div class="col-4 p-2">
                               <p><i class="fas fa-question-circle"></i> Need Help ? Continue us</p>
                           </div>
                           <div class="col-8">
                             <a href="{{ route('product.list') }}"> <button class="btn">Continue Shopping</button></a>
                           </div>
                       </div>
                    </div>

                    <div class="col-6 ">
                        <div class="fixed">
                            <div class="scroll">
                            @foreach($carts as $cart)
                            <div class="row">
                            <div class="col-3">
                                <input type="hidden" name="cartId[]" value="{{ $cart->id }}">
                                <img src="{{ $cart->product_image  }}" class="img-fluid">                            </div>
                            <div class="col-5">
                                <p><b>{{ $cart->product_name }}</b></p>
                                <p class="m-0">{{ $cart->product_color }}</p>
                                <p>{{ $cart->product_size }}</p>
                            </div>
                            <div class="col-4">
                                <div class="orange mt-3">
                                    <p class="mr-3">X{{ $cart->quantity }}</p>
                                    <p>${{ $cart->product_price }} MXN</p>
                                </div>
                            </div>
                            </div>
                            @endforeach
                            </div>
                            <div class="row mt-3 ">
                                <div class="col-8">
                                    <p class="m-0">Subtotal</p>
                                    <p class="m-0">Envio</p>
                                    <p>(Recibelo Manana)</p>
                                </div>
                                <div class="col-4 ">
                                    <p>${{ $subtotal->price  }} MXN</p>
                                    <p>$50 MXN</p>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-8">
                                    <p>Total</p>
                                </div>
                                <div class="col-4">
                                    <p>${{ $subtotal->price + 50 }} MXN</p>
                                </div>
                            </div>

                        </div>

                    </div>
               </div>
           </div>

       </div>
       <!-- dekstop-view  -->


       <!-- mobile view  -->
       <div class="mobile_view">
            <header class="header header-transparent border_bottom">
                <div class="header-middle">
                <div class="container">
                    <div class="header-left">
                    <i class="fas fa-close" style="font-size: 20px;"></i>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="index.html"><img src="{{ asset('assets/img/logo.png') }}" width="150px"></a>
                    </div>
                    <!-- End .header-left -->
                </div>
                <!-- End .container-fluid -->
                </div>
                <!-- End .header-middle -->
            </header>

            <section class="mt-62">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-center">
                            <p><b>Gracias "CLIENT"!</b></p>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12 ">
                            <div class="box">
                                <p><b>Customer Information</b></p>
                                 <div class="row">
                                     <div class="col-6">
                                         <p>Shipping Address</p>
                                         <address>
                                             {{ $user->name }} <br>
                                             {{ $user->street_number }} {{ $user->street }}<br>
                                             {{ $user->state }} {{ $user->postal_code }}<br>
                                             {{ $user->mobile_code }} {{ $user->mobile }}
                                         </address>
                                         <p><b>Shipping Method</b></p>
                                         <p>Expedited Parcel</p>
                                     </div>
                                     <div class="col-6">
                                         <p>Shipping Address</p>
                                         <address>
                                             {{ $user->name }} <br>
                                             {{ $user->street_number }} {{ $user->street }}<br>
                                             {{ $user->state }} {{ $user->postal_code }}<br>
                                             {{ $user->mobile_code }} {{ $user->mobile }}
                                         </address>
                                         <p><b>Payment Method</b></p>
                                         <p>{{ $orderDetails->pay_by }}</p>
                                     </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <p><b>Tu pedido se ha realizado con exito!</b></p>
                            <p><b>Te notificaremos cuando haya sido enviado</b></p>
                            <img src="{{ asset('assets/img/PURCHASE-PROCESS-ON-MOBILE.png') }}" class="img-fluid">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 my-2">
                            <button class="btn btn-block">Regresar a la Tienda</button>
                        </div>
                    </div>
                </div>
            </section>

       </div>
       <!-- mobile view  -->



       <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
       <script src="{{ asset('assets/js/popper.min.js') }}"></script>
       <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
       <script src="{{ asset('assets/js/number.js') }}"></script>
   </body>
</html>
