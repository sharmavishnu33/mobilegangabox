<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <script src='https://kit.fontawesome.com/a076d05399.js'></script>
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/toastr.css') }}">
      <title>Cart</title>
       <style>
           .num input {
               width: 30px;
               text-align: center;
               font-size: 18px;
               color: #a79f9f;
               border: 1px solid rgb(0, 0, 0);
               display: inline-block;
               vertical-align: baseline;
           }
           .num {
               margin-top: 5px;
           }
           span.minus {
               border: 1px solid #000;
               padding: 4px 5px;
               cursor: pointer;
           }
           span.plus {
               border: 1px solid #000;
               padding: 4px 5px;
               display: inline;
               cursor: pointer;
               vertical-align: baseline;
           }
       </style>
   </head>
   <body class="cart">

      <div>
         <header class="header header-transparent">
            <div class="header-middle">
               <div class="container">
                  <div class="header-left">
                     <a href="javascript:history.back()"><i class='fas fa-close ml-2' style="font-size: 20px;"></i></a>&nbsp;&nbsp;

                  </div>
                  <!-- End .header-left -->
                  <a href="#" class="logo">
                     <b><span class="font">Carrito</span></b>
                     </a>
                  <!-- End .header-right -->
               </div>
               <!-- End .container-fluid -->
            </div>
            <!-- End .header-middle -->

         </header>
          <form method="post" action="{{ route('guest.confirm') }}" id="guestConfirm">
              @csrf
         <div class="container-fluid mt-62">
            <div class="row ">
                @foreach($carts as $cart)
               <div class="col-12 mt-4">
                   <div class="dropdown">
                       <i class="fas fa-ellipsis-v dot" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                       <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                           <a class="dropdown-item trash" data-cartid="{{ $cart->id }}">Remove</a>
                       </div>
                   </div>
                  <div class="box">
                      <input type="hidden" id="cart_id{{ $cart->id }}" name="cartid[]" value="{{ $cart->id }}" >
                      <input type="hidden" id="mainprice{{ $cart->id }}" value="{{ $cart->mainPrice }}">
                     <img src="{{ $cart->product_image  }}" width="100%">
                  </div>
                  <p>$<span id="local_product_price{{$cart->id}}">{{ $cart->product_price }}</span> <small>MXN</small></p>
                  <p class="p-0 m-0"><b>{{ $cart->product_name }}</b></p>
                  <p>{{ $cart->product_color }}</p>
                   <div class="num mr-3">
                       <div class="number">
                           <span class="minus removeqnt" data-cartid="{{ $cart->id }}">-</span>
                           <input type="text" value="{{ $cart->quantity }}" id="qnt_{{ $cart->id }}"/>
                           <span class="plus addqnt" data-cartid="{{ $cart->id }}">+</span>
                       </div>
                   </div>
                  <!-- End .product-single-tabs -->

               </div>
                @endforeach
            </div>
            <div class="line_grey mt-3"></div>
            <div class="row py-2">
               <div class="col-8">
                  <div class="form-group m-0">
                     <input type="email" class="form-control" placeholder="Coupon or Voucher">
                     </div>
               </div>
               <div class="col-4 text-center d-flex justify-content-center">
                  <a href="" class="send"><i class="fas fa-arrow-right"></i></a>
               </div>
            </div>
               <div class="line_grey"></div>

            <div class="row mt-3">
               <div class="col-6">
                  <div class="pull-left">
                     <p>Subtotal:</p>
                  </div>
               </div>
               <div class="col-6">
                  <div class="pull-right">
                     <p>$<span id="subtotal">{{ $subtotal->price }}</span> <small>mxn</small></p>
                  </div>
               </div>

            </div>
            <div class="row">
               <div class="col-6">
                  <div class="pull-left">
                     <p>{{ $shoppingDetails->title }}:</p>
                  </div>
               </div>
               <div class="col-6">
                     <div class="pull-right">
                           <p>${{ $shoppingDetails->charge }} <small>mxn</small></p>
                     </div>
               </div>
            </div>
            <hr>
            <div class="row ">
               <div class="col-6 text-center">
                     <p >Total:</p>
               </div>
               <div class="col-6 text-right">
                   <input type="hidden" name="total" id="realtotal" value="{{ $subtotal->price + $shoppingDetails->charge }}">

                   <p class="or">$<b><span id="total">{{ $subtotal->price + $shoppingDetails->charge }} </span><small>mxn</small></b></p>
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <h4 class="text-center"><b>Also you can like this products</b></h4>
               </div>
            </div>
             <input type="hidden" id="lastId" value="{{ $lastProductId }}">
             <input type="hidden" id="previousID">
             <div class="row scrolldown fproductlist" >
                 @foreach($products as $product)
                     <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
                     <div class="col-6 col-md-4 col-lg-3 col-xl-2">
                         <div class="product-default">
                             <figure>
                                 <a href="{{ route('product.description',$productId) }}">
                                     @if(env('APP_ENV') == 'local')

                                         <img class="lazy" src="{{URL::asset('assets/img/PRELOADER.jpg') }}"
                                              data-src="{{env('localUrl').'storage/images/'. $product->images }}">
                                     @else
                                         <img class="lazy" src="{{URL::asset('assets/img/PRELOADER.jpg') }}"
                                              data-src="{{env('serverUrl').'storage/images/'. $product->images }}">
                                     @endif                                 </a>

                             </figure>
                             <div class="product-details">
                                 <h2 class="product-title">
                                     <a href="#">{{ $product->pname }}</a>
                                 </h2>
                                 <div class="price-box">
                                     <span class="product-price"><i>${{ $product->product_price }}</i> MXN</span>
                                 </div>
                                 <!-- End .price-box -->
                                 <div class="category-wrap">
                                     <div class="category-list">
                                         <a href="#" class="product-category">recibelo</a>
                                     </div>
                                 </div>
                             </div>
                             <!-- End .product-details -->
                         </div>
                     </div>
                 @endforeach
             </div>
         </div>
          </form>

         <div class="product-action mt-3">
         <div class="product_price ml-3">
            <span>Total</span>
             <input type="hidden" name="total" id="realtotal" value="{{ $subtotal->price + $shoppingDetails->charge }}">
             <b>$<span id="downtotal">{{ $subtotal->price + $shoppingDetails->charge }}</span>Mxn</b></span>
         </div>
             <a class="paction add-compare" title="Add to compare">
                 <span class="submitForm">Continuar</span>
             </a>
         </div>

      </div>
      <!-- mobile_view  -->

      <div class="mobile-menu-overlay"></div>
      <!-- End .mobil-menu-overlay -->
      <div class="mobile-menu-container">
         <div class="mobile-menu-wrapper">
            <div class="menu-top"> <span><img src="assets/img/logo.png"></span><span class="mobile-menu-close"><img src="assets/img/close.png"></span></div>
            <nav class="mobile-nav">
               <ul class="mobile-menu">
                  <li class="active"><a href="listing.html">Categorías</a></li>
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="caret">Más vendido</span></a>
                     <ul class="dropdown-menu">
                        <li><a href="listing.html">submenu</a></li>
                        <li><a href="listing.html">submenu</a></li>
                        <li><a href="listing.html">submenu</a></li>
                     </ul>
                  </li>
                  <li>
                     <a href="listing.html">Liquidacíon</a>
                  </li>
                  <li>
                     <a href="listing.html">Contactanos</a>
                  </li>
               </ul>
            </nav>
            <!-- End .mobile-nav -->
         </div>
         <!-- End .mobile-menu-wrapper -->
      </div>
      <!-- End .mobile-menu-container -->

      <!-- footer  -->
      <footer>
         <div class="container">
            <div class="row">
               <div class="col-lg-6 col-12">
                  <p>Rasterio de Predidos y dudas sobre nuestros articulos</p>
                  <p>Mensaje de WhatsApp: 55 8732<br>2760</p>
               </div>
               <div class="col-lg-6 col-12">
                  <img src="assets/img/app-store.png" width="150px">
                  <img src="assets/img/google.png" width="150px">
               </div>
            </div>
            <div class="row">
               <div class="col-lg-6"></div>
               <div class="col-lg-6">
                  <p>Menu Inferior</p>
                  <ul>
                     <li><a href="#">Busquesda</a></li>
                     <li><a href="#">Preguntas Frecuentes</a></li>
                     <li><a href="#">Politica de privacidad</a></li>
                     <li><a href="#">Envio Y devoluciones</a></li>
                     <li><a href="#">Quiesnes somos</a></li>
                  </ul>
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <p>Atencion Al Cliente (Cambios)</p>
                  <p>Mensaje de WhatsApp: 55 8029<br>8963</p>
               </div>
            </div>
         </div>
      </footer>
      <!-- footer  -->



      <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
      <script src="{{ asset('assets/js/popper.min.js') }}"></script>
      <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('assets/js/number.js') }}"></script>
      <script src="{{ asset('assets/js/toastr.min.js') }}"></script>

      <script>
          $('.removeqnt').off('click').on('click',function () {

              var _that = $(this);
              var cartId = _that.attr('data-cartid');
              var productPrice = $('#product_price'+cartId).text();
              var mainprice = $('#mainprice'+cartId).val();
              var subtotalprice = $('#subtotal').text();
              var qunty = $('#qnt_'+cartId).val();

              subtotal = parseInt(qunty) * parseInt(mainprice);
              if(qunty > 1) {
                  qunty = parseInt(qunty) - 1;
                  var subtotalprice = $('#subtotal').text();

                  var subtotal = parseInt(qunty) * parseInt(mainprice);
                  var newsubtotal = parseInt(subtotalprice) - parseInt(mainprice);
                  $('#product_price' + cartId).text(subtotal);
                  $('#local_product_price'+cartId).text(subtotal);
                  $('#subtotal').text(newsubtotal);
                  var total = $('#total').text();
                  var mainTotal = parseInt(total) - parseInt(mainprice);
                  $('#total').text(mainTotal);
                  $('#realtotal').text(mainTotal);
                  $('#downtotal').text(mainTotal);

              }
              ajaxCartUpdate(cartId,qunty,subtotal);

          })

          $('.addqnt').off('click').on('click',function () {

              var _that = $(this);
              var cartId = _that.attr('data-cartid');
              var productPrice = $('#product_price'+cartId).text();
              var mainprice = $('#mainprice'+cartId).val();
              var qunty = $('#qnt_'+cartId).val();
              qunty = parseInt(qunty) + 1;
              var subtotalprice = $('#subtotal').text();

              var subtotal = parseInt(qunty) * parseInt(mainprice);
              $('#product_price'+cartId).text(subtotal);
              var newsubtotal = parseInt(mainprice) + parseInt(subtotalprice);
              $('#subtotal').text(newsubtotal);

              $('#product_price'+cartId).text(subtotal);

              $('#local_product_price'+cartId).text(subtotal);
              var total = $('#total').text();
              // alert(total);
              var mainTotal = parseInt(total) + parseInt(mainprice);
              // alert(mainTotal)
              $('#total').text(mainTotal);
              $('#realtotal').text(mainTotal);
              $('#downtotal').text(mainTotal);
              ajaxCartUpdate(cartId,qunty,subtotal);

          })

          function ajaxCartUpdate(cartId,qunty,subtotal) {
              $.ajax(
                  {
                      url: '/update_cart',
                      type: 'POST',
                      data: {
                          _token: "{{ csrf_token() }}",
                          "cartid": cartId,
                          "qunty": qunty,
                          "subtotal": subtotal,
                      },
                      success: function (result) {
                          if (result.success) {
                              return true
                              // toastr.success('Cart updated successfully');
                          } else {
                              $('#qnt_'+cartId).val(parseInt(qunty) - 1);
                              toastr.error('You can add only '+result.quantity+' quantity for this .');
                              return false;
                          }
                      }
                  });
          }

          $('.trash').off('click').on('click',function () {
              var _that = $(this);
              var cartId = _that.attr('data-cartid');
              if (!confirm("Do you want to remove this cart ?")){
                  return false;
              }
              else{
                  $.ajax(
                      {
                          url: '/remove_cart',
                          type: 'POST',
                          data: {
                              _token: "{{ csrf_token() }}",
                              "cartid": cartId,
                          },
                          success: function (result) {
                              if (result.success) {
                                  window.location.reload();
                                  // toastr.success('Cart updated successfully');
                              } else {
                                  return false;
                              }
                          }
                      });
              }

          })

          $('.submitForm').off('click').on('click',function () {
              $('form#guestConfirm').submit();

          })
      </script>

      <script>
          function scrollerData() {

              !function(window){
                  var $q = function(q, res){
                          if (document.querySelectorAll) {
                              res = document.querySelectorAll(q);
                          } else {
                              var d=document
                                  , a=d.styleSheets[0] || d.createStyleSheet();
                              a.addRule(q,'f:b');
                              for(var l=d.all,b=0,c=[],f=l.length;b<f;b++)
                                  l[b].currentStyle.f && c.push(l[b]);

                              a.removeRule(0);
                              res = c;
                          }
                          return res;
                      }
                      , addEventListener = function(evt, fn){
                          window.addEventListener
                              ? this.addEventListener(evt, fn, false)
                              : (window.attachEvent)
                              ? this.attachEvent('on' + evt, fn)
                              : this['on' + evt] = fn;
                      }
                      , _has = function(obj, key) {
                          return Object.prototype.hasOwnProperty.call(obj, key);
                      }
                  ;

                  function loadImage (el, fn) {
                      var img = new Image()
                          , src = el.getAttribute('data-src');
                      img.onload = function() {
                          if (!! el.parent)
                              el.parent.replaceChild(img, el)
                          else
                              el.src = src;

                          fn? fn() : null;
                      }
                      img.src = src;
                  }

                  function elementInViewport(el) {
                      var rect = el.getBoundingClientRect()

                      return (
                          rect.top    >= 0
                          && rect.left   >= 0
                          && rect.top <= (window.innerHeight || document.documentElement.clientHeight)
                      )
                  }

                  var images = new Array()
                      , query = $q('img.lazy')
                      , processScroll = function(){
                          for (var i = 0; i < images.length; i++) {
                              if (elementInViewport(images[i])) {
                                  loadImage(images[i], function () {
                                      images.splice(i, i);
                                  });
                              }
                          };
                      }
                  ;
                  // Array.prototype.slice.call is not callable under our lovely IE8
                  for (var i = 0; i < query.length; i++) {
                      images.push(query[i]);
                  };

                  processScroll();
                  addEventListener('scroll',processScroll);

              }(this);

          }
      </script>

      <script>
          var scroller = true;
          $(document).ready(function () {
              var i = 2;
              $(window).scroll(function () {
                  if ($(window).scrollTop() >= $('.scrolldown').offset().top + $('.scrolldown').outerHeight() - window.innerHeight) {
// ajax call get data from server and append to the div
// if (scroller) {
                      var rout = '{!! Route::currentRouteName() !!}';
                      var lastId = $('#lastId').val();
                      var previousID = $('#previousID').val();
// alert(rout);
                      if (lastId) {
                          if (previousID != lastId) {
                              $('#previousID').val(lastId);
                              $.ajax(
                                  {
                                      url: '{{ route('get.more.data') }}',
                                      type: 'GET',
                                      data: {
                                          "page": i,
                                          'currentriute': rout,
                                          'lastId': lastId
                                      },
                                      success: function (result) {
                                          if (result.success) {
// $('.productslisting').html();

// toastr.success('Cart updated successfully');
                                              $('.fproductlist').append(result.data);
                                              $('#lastId').val(result.lastProductId);

                                          } else {
                                              return false;
                                          }
                                      }
                                  });
                              i = parseInt(i) + 1;
                          }
                      } else {
                          return false;
                      }
                  } else {
                      return false;
                  }
// }
              });
          });
          // function scrollerupdate() {
          //     scroller = false;
          // }
      </script>
   </body>
</html>
