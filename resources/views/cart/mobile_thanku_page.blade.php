<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <script src='https://kit.fontawesome.com/a076d05399.js'></script>
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
      <title>Gangabox | Thanku</title>
   </head>
   <body class="checkout">
       <!-- mobile view  -->
       <div class="mobile_view">
            <header class="header header-transparent border_bottom">
                <div class="header-middle">
                <div class="container-fluid">
                    <div class="header-left">
                    <i class="fas fa-close" style="font-size: 20px;"></i>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="{{ url('/') }}"><img src="{{ asset('assets/img/logo.png') }}" width="150px"></a>
                    </div>
                    <!-- End .header-left -->
                </div>
                <!-- End .container-fluid -->
                </div>
                <!-- End .header-middle -->
            </header>

            <section class="mt-62">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 text-center">
                            <p><b>Gracias " {{ $user->name }}"!</b></p>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12 ">
                            <div class="box">
                                <p><b>Customer Information</b></p>
                                 <div class="row">
                                     <div class="col-6">
                                         <p>Shipping Address</p>
                                         <address>
                                             {{ $user->name }} <br>
                                             {{ $user->street_number }} {{ $user->street }}<br>
                                             {{ $user->state }} {{ $user->postal_code }}<br>
                                             {{ $user->mobile_code }} {{ $user->mobile }}
                                         </address>
                                         <p><b>Shipping Method</b></p>
                                         <p>Expedited Parcel</p>
                                     </div>
                                     <div class="col-6">
                                         <p>Shipping Address</p>
                                         <address>
                                             {{ $user->name }} <br>
                                             {{ $user->street_number }} {{ $user->street }}<br>
                                             {{ $user->state }} {{ $user->postal_code }}<br>
                                             {{ $user->mobile_code }} {{ $user->mobile }}
                                         </address>
                                         <p><b>Payment Method</b></p>
                                         <p>{{ $orderDetails->pay_by }}</p>
                                     </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <p><b>Tu pedido se ha realizado con exito!</b></p>
                            <p><b>Te notificaremos cuando haya sido enviado</b></p>
                            <img src="{{ asset('assets/img/PURCHASE-PROCESS-ON-MOBILE.png') }}" class="img-fluid">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 my-2">
                            <a href="{{ route('product.list') }}" > <button class="btn btn-block">Regresar a la Tienda</button></a>
                        </div>
                    </div>
                </div>
            </section>

       </div>
       <!-- mobile view  -->



       <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
       <script src="{{ asset('assets/js/popper.min.js') }}"></script>
       <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
       <script src="{{ asset('assets/js/number.js') }}"></script>
   </body>
</html>
