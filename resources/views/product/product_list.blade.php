@include('front_layouts.header')
         
         <div class="container-fluid infinite">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                   <div class="sort">
                       <p>Sort By:</p>
                   </div>
                   <div class="select_1">
                       <ul>
                           <li>Recommend</li>
                           <li class="active">Hottest</li>
                           <li>Newest</li>
                           <li>Rating</li>
                           <li>Trending</li>
                       </ul>
                   </div>
                   <div class="price_1">
                       <p>Price <i class="fa fa-sort" aria-hidden="true"></i></p>
                   </div>
                </div>
             </div>
         </div>
         
         <!-- infinite scroll  -->
            <section >
               <div class="container-fluid my-3 infinite">
                    <div class="row">
                      <div class="col-2">
                          <div class="fixed">
                              @foreach($familySubcats as $familySub)
                            <div class="cate">
                                <h4><b>{{ $familySub['fname'] }}</b></h4>
                                    <ul>
                                        @foreach($familySub['subcat'] as $subcat)
                                        <li><a  class="productlist" data-url="{{ route('subcat.products') }}" data-category="{{ $familySub['fid'] }}" data-subcat="{{ $subcat->categories_id }}">{{ $subcat->category->cat_name }}</a></li>
                                            @endforeach

                                    </ul>
                              </div>
                                  @endforeach
                          </div>
                          
                          
                      </div>
                        <div class="col-10">
                            <div class="row" id="productslisting">
                                @foreach($products as $product)
                                    <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
                                <div class="col-sm-3">
                                    <div class="thumbnail">
                                        <a href="{{ route('product.description',$productId) }}">
                                    <img src="{{URL::asset('storage/images/'. $product->images) }}" alt="Fjords" style="width:100%">
                                    <div class="caption">
                                        <p>{{ $product->pname }}</p>
                                        <p >${{ $product->product_price }}<span>MXN</span></p>
                                        <p >Recibelo Manana</p>
                                    </div></a>
                                    </div>
                                </div>
                                @endforeach
                            </div>

                            <!-- <div class="row my-5">
                                <div class="col-12">
                                <div class="text-center">
                                    <button class="see_more">Ver mas</button>
                                </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
               </div>
            </section>
         <!-- infinite scroll  -->
      <!-- </div> -->

       

      <!-- dekstop-view  -->

<div class="mobile_view">
    <div id="myHeader" class="category_top">
        <div class="category_title">
            <div><a class="back" href="index.html"><img src="assets/img/back.png"></a></div>
            <div>
                <h2>Almacenamiiento y organizacion</h2>
                <span class="total product">11 Productors</span>
            </div>
            <div>
                <div class="dropdown cart-dropdown">
                    <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                        <img src="assets/img/mob-cart.png">
                        <!--<span class="cart-count">2</span>-->
                    </a>
                </div>
                <!-- End .dropdown -->
            </div>
        </div>
        <section class="filter_Sec">
            <div class="select-custom">
                <select name="orderby" class="form-control">
                    <option value="menu_order" selected="selected">ordenar</option>
                    <option value="popularity">1</option>
                    <option value="rating">2</option>
                    <option value="date">3</option>
                    <option value="price">4</option>
                    <option value="price-desc">5</option>
                </select>
            </div>
            <div class="filter">
                <span>filtro <img src="assets/img/filter.png"></span>
            </div>
        </section>
    </div>
    <div class="layered-filter-wrapper">
        <div class="mobile-menu-container">
            <div class="filter-menu">
                <div class="menu-top"> <span>FILTER</span><span class="close"><img src="assets/img/close.png"></span></div>
                <nav class="mobile-nav">
                    <ul class="mobile-menu">
                        <li class="active"><a href="index.html">ambietador </a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="caret">bleach</span></a>
                            <ul class="dropdown-menu">
                                <li><a href="">submenu</a></li>
                                <li><a href="">submenu</a></li>
                                <li><a href="">submenu</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">bolsas de basura</a>
                        </li>
                        <li>
                            <a href="#">control de plagas</a>
                        </li>
                        <li>
                            <a href="#">control de plagas</a>
                        </li>
                        <li>
                            <a href="#">cuidado de ropa</a>
                        </li>
                        <li>
                            <a href="#">dientes</a>
                        </li>
                        <li>
                            <a href="#">herramientas de limpieza</a>
                        </li>

                        <li class="active"><a href="index.html">ambietador </a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="caret">bleach</span></a>
                            <ul class="dropdown-menu">
                                <li><a href="">submenu</a></li>
                                <li><a href="">submenu</a></li>
                                <li><a href="">submenu</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">bolsas de basura</a>
                        </li>
                        <li>
                            <a href="#">control de plagas</a>
                        </li>
                        <li>
                            <a href="#">control de plagas</a>
                        </li>
                        <li>
                            <a href="#">cuidado de ropa</a>
                        </li>
                        <li>
                            <a href="#">dientes</a>
                        </li>
                        <li>
                            <a href="#">herramientas de limpieza</a>
                        </li>
                        <li>
                            <a href="">bolsas de basura</a>
                        </li>
                        <li>
                            <a href="#">control de plagas</a>
                        </li>
                        <li>
                            <a href="#">control de plagas</a>
                        </li>
                        <li>
                            <a href="#">cuidado de ropa</a>
                        </li>
                        <li>
                            <a href="#">dientes</a>
                        </li>
                    </ul>
                </nav>
                <!-- End .mobile-nav -->
            </div>
            <!-- End .filter-menu -->
            <div class="select_btn"><a href="#">Seleccionar</a></div>
        </div>
        <!-- End .mobile-menu-container -->
    </div>
    <section class="container-fluid item-product" style="display:block;">
        <div class="row row-sm">
            @foreach($products as $product)
                <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
                <div class="col-6 col-md-4 col-lg-3 col-xl-2">
                    <div class="product-default">
                        <figure>
                            <a href="{{ route('product.description',$productId) }}">
                                <img src="{{URL::asset('storage/images/'. $product->images) }}">
                            </a>
                        </figure>
                        <div class="product-details">
                            <h2 class="product-title">
                                <a href="product.html">{{ $product->pname }}</a>
                            </h2>
                            <div class="price-box">
                                <span class="product-price"><i>${{ $product->product_price }}</i> MXN</span>
                            </div>
                            <!-- End .price-box -->
                            <div class="category-wrap">
                                <div class="category-list">
                                    <p >Recibelo Manana</p>
                                </div></a>
                            </div>
                        </div>
                        <!-- End .product-details -->
                    </div>
                </div>
            @endforeach

        </div>
    </section>
    <div class="filter-overlay"></div>
</div>
     

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
      <script src="{{ asset('assets/js/number.js') }}"></script>
<script>
    $('.productlist').off('click').on('click',function (e) {
        var _that = $(this);
        var _url = _that.attr('data-url');
        var catid = _that.attr('data-category');
        var subcat = _that.attr('data-subcat');
        $.ajax(
            {
                url: _url,
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    "catid": catid,
                    "subcatid": subcat,
                },
                success: function (result) {
                    if (result.success) {
                        $('#productslisting').html('');

                        // var mapData = JSON.parse(result.data);
                        $('#productslisting').html(result.data);
                    } else {
                        return false;
                    }
                }
            });
    })
</script>
   </body>
</html>