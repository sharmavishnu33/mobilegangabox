@include('front_layouts.header')
<style>
    .num input {
        width: 30px;
        text-align: center;
        font-size: 18px;
        color: #a79f9f;
        border: 1px solid rgb(0, 0, 0);
        display: inline-block;
        vertical-align: baseline;
    }
    .num {
        margin-top: 5px;
    }
    span.minus {
        border: 1px solid #000;
        padding: 4px 5px;
        cursor: pointer;
    }
    span.plus {
        border: 1px solid #000;
        padding: 4px 5px;
        display: inline;
        cursor: pointer;
        vertical-align: baseline;
    }
</style>
<section class="dekstop_slider mb-3">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-lg-6">
                     <div class="fixed">
                        <div class="row">
                           <div class="col-md-2 col-4">
                              <div class="box">
                                 @foreach($varients as $productImg)
                                    @if($productImg->productImage->images)
                                 <div class="box_1 image_data" data-url="{{ route('image.view') }}" data-imageid="{{$productImg->productImage->id}}">
                                     <div class="over_cover"></div>
{{--                                    <div class="over_cover">+7</div>--}}
                                     <span class="" >
                                         <img src="{{URL::asset('storage/images/'. $productImg->productImage->images) }}" class="img-fluid"></span>
                                 </div>
                                      @endif
                                    @endforeach
                              </div>
                           </div>
                           <div class="col-md-10 col-8 p-3">
                              <div class="text-center">
                                 <div class="large replace_image">
                                     <?php $i =0;?>
                                     @foreach($varients as $productImg)
                                             @if ($loop->first)
                                         @if($productImg->productImage->images)
                                             <?php $i++; ?>
                                    <img src="{{URL::asset('storage/images/'. $productImg->productImage->images) }}" class="img-fluid">
                                         @endif
                                         @endif
                                         @endforeach
                                 </div>
                              </div>
                              
                           </div>
                        </div>
                     </div>
                     
                  </div>
                  <div class="col-lg-6">

                     <h2>{{ $pvariant->name }}</h2>
                     <form>
                        <div class="form-row">
                           <div class="form-group col-md-6">
                              <label for="inputState" class="grey">Color:</label>
                              <select id="inputState" class="form-control product_colors" data-url="{{ route('product.size') }}" data-productId="{{ $pvariant->id }}">
                                 <option>Select Color</option>
                                @foreach($varients as $productColor)
                              <option value="{{ $productColor->colorData->id }}"> {{ $productColor->colorData->color_name }}</option>
                             @endforeach
                              </select>
                           </div>
                           <div class="form-group col-md-6 size_details">
                              <label for="inputState" class="grey">Talla:</label>
                              <select id="inputState" class="form-control color_size" disabled="">
                                 <option>Select talla</option>
                                 <option value="XS">XS</option>
                                 <option value="S">S</option>
                                 <option value="M">M</option>
                                 <option value="L" >L</option>
                                 <option value="XL">XL</option>
                                 <option value="XXL">XXL</option>
                                 <option value="3XL">3XL</option>
                              </select>
                           </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-2 p-0">
                                <div class="num">
                                    <div class="number">
                                        <span class="minus removeqnt">-</span>
                                        <input type="text" id="quantity" name="p_qnt" value="1"/>
                                        <span class="plus addqnt">+</span>
                                    </div>
                                </div>
                            </div>
{{--                           <div class="form-group col-md-2">--}}
{{--                            <label for="inputZip "><span class="removeqnt">minus</span></label>--}}
{{--                           <input type="hidden" class="form-control" id="quantity" name="p_qnt" value="1">--}}
{{--                               <label for="inputZip "><span class="addqnt">plush</span></label>--}}
{{--                        </div>--}}

                        <div class="form-group col-md-10">
                            <button type="button" class="sign addtocart" data-url="{{ route('add.cart') }}"  data-pid="{{ $pvariant->id }}">Comprar</button>
                        </div>
                        </div>
                        
                        
                     </form>
                      <!-- End .price-box -->
                      <div class="product-desc">
                        <ul>
                           <li class="mb-2">Recibelo manana
                           </li>
                           <li
                           class="mb-2">15 dias de garantia
                           </li>
                           <li class="mb-2">Pagalo en efectivo al recibir
                           </li>
                           <li class="mb-2">Cambios y devoluciones sincosto
                           </li>
                        </ul>
                     </div>
                     <!-- End .product-desc -->
                     <div class="list_1 mt-5">
                         <p><b><?php echo $pvariant->size_desc;?></b></p>
                         <p></p>
                         <?php echo $pvariant->description;?>
{{--                         {{ $desc }}--}}
{{--                        {{  strip_tags(preg_replace('/\s|&nbsp;/', '', $pvariant->description)) }}--}}
                     </div>
                     <!-- end list  -->
                     @foreach($productImage as $pimage)
                     <div class="img">
                        <img src="{{URL::asset('storage/images/'. $pimage->image) }}" class="img-fluid">
                     </div>

                        @endforeach
                  </div>
               </div>
            </div>
         </section>
         <!-- infinite scroll  -->
            <section>
               <div class="container my-3 infinite">
                  <div class="row">
                     <div class="col-12">
                        <div class="text-center">
                           <h5><b>Articulos similares:</b></h5>
                        </div>
                        <div class="line"></div>
                     </div>
                  </div>
                  <div class="row">
                     @foreach($products as $product)
                        <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
                        <div class="col-sm-3">
                           <div class="thumbnail">
                              <a href="{{ route('product.description',$productId) }}">
                                 <img src="{{URL::asset('storage/images/'. $product->images) }}" alt="Fjords" style="width:100%">
                                 <div class="caption">
                                    <p>{{ $product->pname }}</p>
                                    <p >${{ $product->product_price }}<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                 </div></a>
                           </div>
                        </div>
                     @endforeach

                  </div>
                  <div class="row my-5">
                     <div class="col-12">
                        <div class="text-center">
                           <button class="see_more">Ver mas</button>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         <!-- infinite scroll  -->
      </div>

       

      <!-- dekstop-view  -->
      
      <header class="header header-transparent">
         <div class="header-middle">
            <div class="container">
               <div class="header-left">
                  <button class="mobile-menu-toggler" type="button">
                  <!-- <img src="assets/img/mob-menu.png"> -->
                  </button>
                  <i class='fas fa-arrow-left' style="color:#FF7100;font-size: 20px;"></i>&nbsp;&nbsp;
                  <select class="currency-selector">
                     <option selected>CDMX</option>
                     <option >ABC</option>
                  </select>
               </div>
               <!-- End .header-left -->
			       <a href="index.html" class="logo">
                  <img src="{{ asset('assets/img/logo.png') }}" alt="Logo">
                  </a>
               <div class="header-right">
                  <a href="login.html">
                     <div class="header-user">
                        <img src="{{ asset('assets/img/mob-user.png') }}">
                     </div>
                  </a>
                  <div class="dropdown cart-dropdown">
                     <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                        <img src="{{ asset('assets/img/mob-cart.png') }}">
                        <!--<span class="cart-count">2</span>-->
                     </a>
                  </div>
                  <!-- End .dropdown -->
                  <div class="header-search">
                     <a href="#" class="search-toggle" role="button"><img src="{{ asset('assets/img/mob-search.png') }}"></a>
                     <!-- <form action="#" method="get">
                        <div class="header-search-wrapper">
                            <input type="search" class="form-control" name="q" id="q" placeholder="I'm searching for..." required="">
                           
                            <button class="btn" type="submit"><i class="icon-search-3"></i></button>
                        </div>
                        </form>-->
                  </div>
               </div>
               <!-- End .header-right -->
            </div>
            <!-- End .container-fluid -->
         </div>
         <!-- End .header-middle -->
         
      </header>
      <div class="container-fluid mt-5">
         <div class="row">
            <div class="col-12">
               <div class="product-single-container">
                  <div class="row">
                     <div class="product-single-gallery">
                        <div class="product-slider-container product-item">
                            <!-- slider -->
                           <div id="demo" class="carousel slide" data-ride="carousel">
                              <ul class="carousel-indicators">
                                  <?php $k=0; ?>
                                  @foreach($productImage as $pimage)
                                 <li data-target="#demo" data-slide-to="{{ $k }}" class="{{ $k==0 ? 'active' :'' }}"></li>
                                      <?php $k++; ?>
                                      @endforeach
                              </ul>
                              <div class="carousel-inner">
                                  <?php $m=0; ?>
                                  @foreach($productImage as $pimage)
                                 <div class="carousel-item {{ $m == 0 ?'active' :'' }}">
                                    <img src="{{URL::asset('storage/images/'. $pimage->image) }}" alt="banner" class="img-fluid">
                                    <!-- <div class="carousel-caption">
                                       <h3>Los Angeles</h3>
                                       <p>We had such a great time in LA!</p>
                                       </div>-->   
                                 </div>
                                          <?php $m++; ?>
                                      @endforeach

                              </div>
                              <a class="carousel-control-prev" href="#demo" data-slide="prev">
                              <span class="carousel-control-prev-icon"></span>
                              </a>
                              <a class="carousel-control-next" href="#demo" data-slide="next">
                              <span class="carousel-control-next-icon"></span>
                              </a>
                           </div>
                           <!-- slider -->
                           <!-- End .product-single -->
                           <span class="prod-full-screen">
                           <i class="icon-plus"></i>
                           </span>
                        </div>
                     </div>
                     <div class="col-12">
                        <div class="product-single-details">
                           <h1 class="product-title">{{ $pvariant->name }}</h1>
                           <div class="price-box">
                              <span class="product-price">MXN${{ $pvariant->productVariant->product_price }}</span>
                           </div>
                           <!-- End .price-box -->
                           <div class="product-desc">
                              <ul>
                                 <li>recibelo manana
                                 </li>
                                 <li>recibelo manana
                                 </li>
                                 <li>recibelo manana
                                 </li>
                                 <li>recibelo manana
                                 </li>
                              </ul>
                           </div>
                           <!-- End .product-desc -->
                           
                        </div>
                        <!-- End .product-single-details -->
                     </div>
                     <!-- End .col-lg-5 -->
                     <div class="product-action mt-3">
                        <div class="product_price">
                           <span>MXN${{ $pvariant->productVariant->product_price }}</span>
                           <span>recibelo manana</span>
                        </div>
                        <!-- End .product-single-qty -->
                        <a class="paction add-cart addtocart" title="Add to Cart" data-url="{{ route('add.cart') }}"  data-pid="{{ $pvariant->id }}">
                        <span><img class="crt" src="{{ asset('assets/img/inferior.png') }}"/></span>
                        <span>carrito</span>
                        </a>
                        <a href="details-2.html" class="paction add-compare" title="Add to compare">
                        <span>comprar</span>
                        </a>
                     </div>
                     <!-- End .product-action -->
                  </div>
                  <!-- End .row -->
               </div>
               
               <!-- End .product-single-container -->
               <div class="product-single-tabs row">
                  <ul class="nav nav-tabs" role="tablist">
                     <li class="nav-item">
                        <a class="nav-link active" id="product-tab-desc" data-toggle="tab" href="#product-desc-content" role="tab" aria-controls="product-desc-content" aria-selected="true">Description</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" id="product-tab-spec" data-toggle="tab" href="#product-spec-content" role="tab" aria-controls="product-spec-content" aria-selected="false">specification</a>
                     </li>
                  </ul>
                  <div class="tab-content">
                     
                     <div class="tab-pane fade show active" id="product-desc-content" role="tabpanel" aria-labelledby="product-tab-desc">
                        <div class="product-desc-content">
                            <?php echo $pvariant->description;?>
                                @foreach($productImage as $pimage)
{{--                                    <div class="img">--}}
                                        <img src="{{URL::asset('storage/images/'. $pimage->image) }}" class="img-fluid">
{{--                                    </div>--}}
                                    @endforeach
                        </div>
                        <!-- End .product-desc-content -->
                     </div>
                     <!-- End .tab-pane -->
                     <div class="tab-pane fade" id="product-spec-content" role="tabpanel" aria-labelledby="product-tab-spec">
                        <div class="product-spec-content">
                            <?php echo $pvariant->specification;?>
                        </div>
                        <!-- End .product-spec-content -->
                     </div>
                     <!-- End .tab-pane -->
                  </div>
                  <!-- End .tab-content -->
               </div>
               <!-- End .product-single-tabs -->
            </div>
         </div>
      </div>


       <!-- popup -->
       <div class="overlayer" style="display: none;">
         <div class="popup">
            <div class="login">
               <section >
                  <div class="header-middle">
                     <div class="container">
                        <div class="row align-items-center">
                           <div class="col-4">
                              <div class="header-left">
                                 <i class='fas fa-times ml-2 ' id="hide" style="font-size: 20px;"></i>
                              </div>
                           </div>
                           <div class="col-8">
                              <a href="{{ url('/') }}" class="logo">
                                 <img src="{{ asset('assets/img/logo.png') }}" alt="Logo">
                                 </a>
                           </div>
                        </div>
                        
                        <!-- End .header-left -->
                        
                        <!-- End .header-right -->
                     </div>
                     <!-- End .container-fluid -->
                  </div>
                 <!-- End .header-middle -->
               </section>
               <div class="container-fluid mt-3">
                  
                  <div class="row">
                     <div class="col-12">
                         <ul class="nav nav-tabs tab_1">
                             <li class="active"><a data-toggle="tab" href="#home">INGRESAR</a></li>
                             <li><a data-toggle="tab" href="#menu1">REGISTRARSE</a></li>
                           </ul>
         
                           <div class="tab-content">
                             <div id="home" class="tab-pane in active">
                                 <form class="form">
                                     <div class="row">
                                         <div class="col-12">
                                             <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="Correo electronico">
                                         </div>
                                         <div class="col-12">
                                             <div class="input-group mb-2 mr-sm-2">
                                             <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Contrasena">
                                             </div>
                                         </div>
                                         
                                     </div>
                                     <div class="row my-3">
                                         <div class="col-12 text-center">
                                             <a href="#" class="orange">Iniciar sesion</a>
                                         </div>
                                     </div>
                                 </form>
                             </div>
                             <div id="menu1" class="tab-pane">
                                 <form class="form">
                                     <div class="row">
                                         <div class="col-12">
                                             <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="Correo electronico">
                                         </div>
                                         <div class="col-12">
                                             <div class="input-group mb-2 mr-sm-2">
                                             <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Contrasena">
                                             </div>
                                         </div>
                                         
                                     </div>
                                     <div class="row my-3">
                                         <div class="col-12 text-center">
                                             <a href="#" class="orange">REGISTRARSE</a>
                                         </div>
                                     </div>
                                 </form>
                             </div>
                             
                           </div>
                         
                           <div class="row my-3">
                             <div class="col-12 text-center line">
                                 <p><span class="pull-left"></span>o<span class="pull-right"></span></p>
                             </div>
                         </div>
                         <div class="row my-3">
                             <div class="col-12 text-center">
                                 <a href="#" class="facebook"><i class="fab fa-facebook-square"></i> Iniciar sesion con Facebook</a>
                             </div>
                         </div>
                         <div class="row my-3">
                             <div class="col-12 text-center">
                                 <a href="#" class="gmail">
                                     <img src="assets/img/gmail.png" width="30px" style="margin-top: -6px;">
                                     <span>Iniciar sesion con Gmail</span>
                                 </a>
                             </div>
                         </div>
                         <div class="row my-3">
                             <div class="col-12 text-center grey">
                                 <p>AI inciar sesion usted acepta <br> los <span class="red"><a href="#">treminos y condiciones y politicas de privacidad</a></span> </p>
                             </div>
                         </div>
                     </div>
                  </div>
               </div>
            </div>
            
         </div>
      </div>
   <!-- popup -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
      <script src="{{ asset('assets/js/number.js') }}"></script>
     <script src="{{ asset('assets/js/toastr.min.js') }}"></script>

<script>
    $('.image_data').off('click').on('click',function (e) {
        var _that = $(this);
        var _url = _that.attr('data-url');
        var imageid = _that.attr('data-imageid');
        $.ajax(
            {
                url: _url,
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    "imageid": imageid,
                },
                success: function (result) {
                    if (result.success) {
                        $('.replace_image').html('');
                        // var mapData = JSON.parse(result.data);
                        $('.replace_image').html(result.data);
                    } else {
                        return false;
                    }
                }
            });
    })

    $('.product_colors').off('change').on('change',function (e) {
        var _that = $(this);
        var _url = _that.attr('data-url');
        var productId = _that.attr('data-productId');
        var color =_that.val();
        $.ajax(
            {
                url: _url,
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    "color": color,
                    "productId": productId,
                },
                success: function (result) {
                    if (result.success) {
                        $('.size_details').html('');
                        // var mapData = JSON.parse(result.data);
                        $('.size_details').html(result.data);
                    } else {
                        return false;
                    }
                }
            });
    })

    $('.addtocart').off('click').on('click',function (e) {
        var _that = $(this);
        var _url = _that.attr('data-url');
        var productId = _that.attr('data-pid');
        var colorid = $('.product_colors').val();
        var colorsize = $('.color_size').val();
        var quanti = $('#quantity').val();
        // alert(quanti);
        if(colorid == 'Select Color' )
        {
            toastr.error('Please select color');
            return false;
        }
        if(colorsize == 'Select talla' || colorsize =='Select size'){
            toastr.error('Please select size');
            return false;
        }
        $.ajax(
            {
                url: _url,
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    "colorId": colorid,
                    "colorSize": colorsize,
                    "quanti": quanti,
                    "productId": productId,
                },
                success: function (result) {
                    if (result.success) {
                        toastr.success('Cart add successfully');
                        // $('.size_details').html('');
                        // // var mapData = JSON.parse(result.data);
                        // $('.size_details').html(result.data);
                    } else {
                        return false;
                    }
                }
            });
    })
</script>
   </body>
</html>