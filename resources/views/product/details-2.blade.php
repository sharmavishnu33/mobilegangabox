<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src='https://kit.fontawesome.com/a076d05399.js'></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <link rel="stylesheet" href="assets/css/style.css">
      <title>Document</title>
   </head>
   <body class="varient_bar">
      <header class="header header-transparent">
         <div class="header-middle">
            <div class="container">
               <div class="header-left">
                  <button class="mobile-menu-toggler" type="button">
                  <!-- <img src="assets/img/mob-menu.png"> -->
                  </button>
                  <a href="details.html"><i class='fas fa-arrow-left' style="color:#FF7100;font-size: 20px;"></i></a>&nbsp;&nbsp;
                  <select class="currency-selector">
                     <option selected>CDMX</option>
                     <option >ABC</option>
                  </select>
               </div>
               <!-- End .header-left -->
			       <a href="index.html" class="logo">
                  <img src="assets/img/logo.png" alt="Logo">
                  </a>
               <div class="header-right">
                  <a href="login.html">
                     <div class="header-user">
                        <img src="assets/img/mob-user.png">
                     </div>
                  </a>
                  <div class="dropdown cart-dropdown">
                     <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                        <img src="assets/img/mob-cart.png">
                        <!--<span class="cart-count">2</span>-->
                     </a>
                  </div>
                  <!-- End .dropdown -->
                  <div class="header-search">
                     <a href="#" class="search-toggle" role="button"><img src="assets/img/mob-search.png"></a>
                     <!-- <form action="#" method="get">
                        <div class="header-search-wrapper">
                            <input type="search" class="form-control" name="q" id="q" placeholder="I'm searching for..." required="">

                            <button class="btn" type="submit"><i class="icon-search-3"></i></button>
                        </div>
                        </form>-->
                  </div>
               </div>
               <!-- End .header-right -->
            </div>
            <!-- End .container-fluid -->
         </div>
         <!-- End .header-middle -->

      </header>
      <div class="overlay clos"></div>
      <div class="container-fluid mt-5">
        <div class="row">
           <div class="col-12">
              <div class="product-single-container">
                 <div class="row ">
                    <div class="product-single-gallery">
                       <div class="product-slider-container product-item">
                           <!-- slider -->
                          <div id="demo" class="carousel slide" data-ride="carousel">
                             <ul class="carousel-indicators">
                                <li data-target="#demo" data-slide-to="0" class="active"></li>
                                <li data-target="#demo" data-slide-to="1"></li>
                                <li data-target="#demo" data-slide-to="2"></li>
                             </ul>
                             <div class="carousel-inner">
                                <div class="carousel-item active">
                                   <img src="assets/img/solar.png" alt="banner" class="img-fluid">
                                   <!-- <div class="carousel-caption">
                                      <h3>Los Angeles</h3>
                                      <p>We had such a great time in LA!</p>
                                      </div>-->
                                </div>
                                <div class="carousel-item">
                                   <img src="assets/img/solar.png" alt="banner" class="img-fluid">
                                   <!--<div class="carousel-caption">
                                      <h3>Chicago</h3>
                                      <p>Thank you, Chicago!</p>
                                      </div> -->
                                </div>
                                <div class="carousel-item">
                                   <img src="assets/img/solar.png" alt="banner" class="img-fluid">
                                   <!--<div class="carousel-caption">
                                      <h3>New York</h3>
                                      <p>We love the Big Apple!</p>
                                      </div>-->
                                </div>
                             </div>
                             <a class="carousel-control-prev" href="#demo" data-slide="prev">
                             <span class="carousel-control-prev-icon"></span>
                             </a>
                             <a class="carousel-control-next" href="#demo" data-slide="next">
                             <span class="carousel-control-next-icon"></span>
                             </a>
                          </div>
                          <!-- slider -->
                          <!-- End .product-single -->
                          <span class="prod-full-screen">
                          <i class="icon-plus"></i>
                          </span>
                       </div>
                    </div>
                    <div class="col-12">
                       <div class="product-single-details">
                          <h1 class="product-title">lampara solar</h1>
                          <div class="price-box">
                             <span class="product-price">MXN$99</span>
                          </div>
                          <!-- End .price-box -->
                          <div class="product-desc">
                             <ul>
                                <li>recibelo manana
                                </li>
                                <li>recibelo manana
                                </li>
                                <li>recibelo manana
                                </li>
                                <li>recibelo manana
                                </li>
                             </ul>
                          </div>
                          <!-- End .product-desc -->

                       </div>
                       <!-- End .product-single-details -->
                    </div>
                    <!-- End .col-lg-5 -->
                    <div class="product-action clos">
                        <div class="display relate">
                             <div class="name">
                                 <div class="box">
                                     <img src="assets/img/solar.png" width="100%" alt="img">
                                 </div>
                                 <p class="product-price">MXN$49.00</p>
                                 <p><b>Seleccion: AZUL</b></p>
                             </div>
                             <div class="sticker">
                                 <p>Quick<br>
                                 guide</p>
                             </div>
                             <i class="fas fa-close " id="hide"></i>
                        </div>

                        <div class="display">
                            <div class="color">
                                 <p><b>COLOR:&nbsp;</b>
                                     <span>AZUL</span>&nbsp;
                                     <span>ROSA</span>&nbsp;
                                     <span>NEGRO</span>&nbsp;
                                     <span>PLATA</span>&nbsp;
                                     <span>ORO</span>
                                 </p>
                             </div>
                        </div>

                        <div class="display">
                                <div class="product_price">
                                     <div class="number">
                                         <span class="minus">-</span>
                                         <input type="text" value="1"/>
                                         <span class="plus">+</span>
                                     </div>
                                 </div>
                                 <!-- <div class="product_price">
                                    <span>MXN$99</span>
                                    <span>recibelo manana</span>
                                 </div> -->
                                 <div class="carito">
                                     <a href="cart.html"><img src="assets/img/inferior.png" class="pt-2" width="22px">
                                     <p class="m-0"><b>Carrito</b></p>
                                 </div>
                                   <a href="cart.html" class="paction add-compare" title="Add to compare">
                                      <span>comprar</span>
                                   </a>
                        </div>

                       <!-- End .product-single-qty -->


                    </div>
                    <!-- End .product-action -->
                 </div>
                 <!-- End .row -->
              </div>

              <!-- End .product-single-container -->
              <div class="product-single-tabs row">
                 <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                       <a class="nav-link active" id="product-tab-desc" data-toggle="tab" href="#product-desc-content" role="tab" aria-controls="product-desc-content" aria-selected="true">Description</a>
                    </li>
                    <li class="nav-item">
                       <a class="nav-link" id="product-tab-spec" data-toggle="tab" href="#product-spec-content" role="tab" aria-controls="product-spec-content" aria-selected="false">specification</a>
                    </li>
                 </ul>
                 <div class="tab-content">

                    <div class="tab-pane fade show active" id="product-desc-content" role="tabpanel" aria-labelledby="product-tab-desc">
                       <div class="product-desc-content">
                          <img src="assets/img/solar1.png" class="img-fluid">
                       </div>
                       <!-- End .product-desc-content -->
                    </div>
                    <!-- End .tab-pane -->
                    <div class="tab-pane fade" id="product-spec-content" role="tabpanel" aria-labelledby="product-tab-spec">
                       <div class="product-spec-content">
                          <ul>
                          <li>1 x sensor de movimiento PIR solar luz</li>
                          <li>1 x tornillos</li>
                          <li>1 x pin de llave</li>
                          <li>1 x Pilar de expansión-bisagra</li>
                          <li>1 x Manual del usuario</li>
                       </div>
                       <!-- End .product-spec-content -->
                    </div>
                    <!-- End .tab-pane -->
                 </div>
                 <!-- End .tab-content -->
              </div>
              <!-- End .product-single-tabs -->
           </div>
        </div>
     </div>

      <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
      <script src="{{ asset('assets/js/popper.min.js') }}"></script>
      <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('assets/js/number.js') }}"></script>
   </body>
</html>
