@foreach($products as $product)
    <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
    <div class="col-sm-3">
        <div class="thumbnail">
            <a href="{{ route('product.description',$productId) }}">
                <img src="{{URL::asset('storage/images/'. $product->images) }}" alt="Fjords" style="width:100%">
                <div class="caption">
                    <p>{{ $product->pname }}</p>
                    <p >${{ $product->product_price }}<span>MXN</span></p>
                    <p >Recibelo Manana</p>
                </div></a>
        </div>
    </div>
@endforeach