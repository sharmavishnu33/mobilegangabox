@extends('layout.layout')
@section('content')
      <!-- mobile view  -->
      <div class="mobile_view">
          <div id="myHeader" class="category_top">
        <div class="category_title">
           <div><a class="back" href="{{ url('/') }}"><img src="{{ asset('assets/img/back.png') }}"></a></div>
           <div>
              <h2>Almacenamiiento y organizacion</h2>
              <span class="total product">11 Productors</span>
           </div>
           <div>
              <div class="dropdown cart-dropdown">
                 <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                    <img src="assets/img/mob-cart.png">
                    <!--<span class="cart-count">2</span>-->
                 </a>
              </div>
              <!-- End .dropdown -->
           </div>
        </div>
        <section class="filter_Sec">
           <div class="select-custom">
              <select name="orderby" class="form-control">
                 <option value="menu_order" selected="selected">ordenar</option>
                 <option value="popularity">1</option>
                 <option value="rating">2</option>
                 <option value="date">3</option>
                 <option value="price">4</option>
                 <option value="price-desc">5</option>
              </select>
           </div>
           <div class="filter">
              <span>filtro <img src="{{ asset('assets/img/filter.png') }}"></span>
           </div>
        </section>
            </div>

            <section class="container-fluid item-product scrolldown">
                <input type="hidden" id="lastId" value="{{ $lastProductId }}">
                <input type="hidden" id="previousID">
                <div class="row row-sm fproductlist" >
                    @foreach($products as $product)
                        <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
                        <div class="col-6 col-md-4 col-lg-3 col-xl-2">
                            <div class="product-default">
                                <figure>
                                    <a href="{{ route('product.description',$productId) }}">
                                        @if(env('APP_ENV') == 'local')

                                            <img class="lazy" src="{{URL::asset('assets/img/PRELOADER.jpg') }}"
                                                 data-src="{{env('localUrl').'storage/images/'. $product->images }}">
                                        @else
                                            <img class="lazy" src="{{URL::asset('assets/img/PRELOADER.jpg') }}"
                                                 data-src="{{env('serverUrl').'storage/images/'. $product->images }}">
                                        @endif
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <h2 class="product-title">
                                        <a href="{{ route('product.description',$productId) }}">{{ $product->pname }}</a>
                                    </h2>
                                    <div class="price-box">
                                        <span class="product-price"><i>${{ $product->product_price }}</i> MXN</span>
                                    </div>
                                    <!-- End .price-box -->
                                    <div class="category-wrap">
                                        <div class="category-list">
                                            <a href="#" class="product-category">recibelo</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- End .product-details -->
                            </div>
                        </div>
                    @endforeach
                </div>

            </section>
            <div class="filter-overlay"></div>
      </div>

@endsection

