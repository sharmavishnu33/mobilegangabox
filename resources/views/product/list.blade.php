<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/style1.css') }}">
    <title>Gangabox | Product_list</title>
</head>
<body>
<!-- dekstop-view  -->
<!-- <div class="dekstop"> -->
<div class="dekstop_view">
    <section class="sticky-top pb-4">
        <div class="container desk-header ">
            <div class="row">
                <div class="col-lg-3 col-4 pr-0 ">
                    <button class="mobile-menu-toggler" type="button">
                        <img src="assets/img/mob-menu.png">
                    </button>
                    <img src="assets/img/logo.png" width="150px">
                </div>
                <div class="col-lg-5 col-6">
                    <div class="search-box">
                        <input class="input-text" type="text" placeholder="Search entire store here...">
                        <button class="search-btn"><i class="fa fa-search"></i></button>
                    </div>
                    <p class="txt_2">Palabras populares: Relojes  &nbsp;<span>Organizacion</span>  &nbsp;<span>Mascarillas</span></p>
                </div>
                <div class="col-lg-3 col-2 cent">
                    <div class="text-right">
                        <ul class="list">
                            <li class="acco"><img src="assets/img/user.png" > Mi Cuenta</li>
                            <li><img src="assets/img/trollie.png" > Carrito</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container-fluid infinite">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-10">
                <div class="sort">
                    <p>Sort By:</p>
                </div>
                <div class="select_1">
                    <ul>
                        <li>Recommend</li>
                        <li class="active">Hottest</li>
                        <li>Newest</li>
                        <li>Rating</li>
                        <li>Trending</li>
                    </ul>
                </div>
                <div class="price_1">
                    <p>Price <i class="fa fa-sort" aria-hidden="true"></i></p>
                </div>
            </div>
        </div>
    </div>

    <!-- infinite scroll  -->
    <section>
        <div class="container-fluid my-3 infinite">
            <div class="row">
                <div class="col-2">
                    <div class="fixed">
                        <div class="cate">
                            <h4><b>Categories</b></h4>
                            <ul>
                                <li>Sub</li>
                                <li>Sub</li>
                                <li>Sub</li>
                            </ul>
                        </div>

                        <div class="cate my-3">
                            <h4><b>Categories</b></h4>
                            <ul>
                                <li>Sub</li>
                                <li>Sub</li>
                                <li>Sub</li>
                            </ul>
                        </div>

                        <div class="cate">
                            <h4><b>Categories</b></h4>
                            <ul>
                                <li>Sub</li>
                                <li>Sub</li>
                                <li>Sub</li>
                            </ul>
                        </div>
                    </div>


                </div>
                <div class="col-10">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-4.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Cajas de zapto multifunction</p>
                                    <p >$39<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-3.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Malla adherible para mosquito</p>
                                    <p >$99<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-6.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Tapas flexibles para aliement</p>
                                    <p >$49<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-7.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Reloj retro vintage diferentes</p>
                                    <p >$119<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row my-4">
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-4.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Cajas de zapto multifunction</p>
                                    <p >$39<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-3.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Malla adherible para mosquito</p>
                                    <p >$99<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-6.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Tapas flexibles para aliement</p>
                                    <p >$49<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-7.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Reloj retro vintage diferentes</p>
                                    <p >$119<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row my-4">
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-4.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Cajas de zapto multifunction</p>
                                    <p >$39<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-3.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Malla adherible para mosquito</p>
                                    <p >$99<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-6.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Tapas flexibles para aliement</p>
                                    <p >$49<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-7.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Reloj retro vintage diferentes</p>
                                    <p >$119<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row my-4">
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-4.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Cajas de zapto multifunction</p>
                                    <p >$39<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-3.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Malla adherible para mosquito</p>
                                    <p >$99<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-6.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Tapas flexibles para aliement</p>
                                    <p >$49<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-7.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Reloj retro vintage diferentes</p>
                                    <p >$119<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-4.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Cajas de zapto multifunction</p>
                                    <p >$39<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-3.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Malla adherible para mosquito</p>
                                    <p >$99<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-6.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Tapas flexibles para aliement</p>
                                    <p >$49<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/p-7.png" alt="Fjords" style="width:100%">
                                <div class="caption">
                                    <p>Reloj retro vintage diferentes</p>
                                    <p >$119<span>MXN</span></p>
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row my-5">
                        <div class="col-12">
                        <div class="text-center">
                            <button class="see_more">Ver mas</button>
                        </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    <!-- infinite scroll  -->
    <div class="mobile-menu-overlay"></div>
    <div class="mobile-menu-container">
        <div class="mobile-menu-wrapper">
            <div class="menu-top"> <span><img src="assets/img/logo.png"></span><span class="mobile-menu-close"><img src="assets/img/close.png"></span></div>
            <nav class="mobile-nav">
                <ul class="mobile-menu">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="caret">Categorías</span></a>
                        <ul class="dropdown-menu">
                            <li><a href="listing.html">submenu</a></li>
                            <li><a href="listing.html">submenu</a></li>
                            <li><a href="listing.html">submenu</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="listing.html">Más vendido</a>
                    </li>
                    <li>
                        <a href="listing.html">Liquidacíon</a>
                    </li>
                    <li>
                        <a href="listing.html">Contactanos</a>
                    </li>
                </ul>
            </nav>
            <!-- End .mobile-nav -->
        </div>
        <!-- End .mobile-menu-wrapper -->
    </div>
</div>


<!-- </div> -->




<!-- dekstop-view  -->

<!-- mobile view  -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="assets/js/number.js"></script>
<script>
    $('.filter span').on('click', function (e) {
        $('body').toggleClass('mmenu-active');
        $(this).toggleClass('active');
        e.preventDefault();
    });

    $('.filter-overlay, .close').on('click', function (e) {
        $('body').removeClass('mmenu-active');
        e.preventDefault();
    });
</script>
<script>
    window.onscroll = function() {myFunction()};

    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }
</script>

<!-- mobile view  -->
</body>
</html>