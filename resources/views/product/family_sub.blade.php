        <ul class="mobile-menu">
            <?php $b=0;  ?>
            @foreach($familySubcats as $familySub)
            <li class="{{ $b == 0 ? 'dropdown': ''}}">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="caret">{{ $familySub['fname'] }}</span></a>
                <ul class="dropdown-menu">
                    @if(count($familySub['subcat']) > 0)
                    @foreach($familySub['subcat'] as $subcat)
                        <?php $fid = \App\Traits\CommonTrait::encodeId($familySub['fid']); $subId = \App\Traits\CommonTrait::encodeId($subcat->categories_id) ?>
                    <li><a href="{{ route('family.product.list',[$subId,$fid]) }}">{{ $subcat->category->cat_name }}</a></li>
                        @endforeach
                        @endif
                </ul>
            </li>
                    <?php $b++; ?>
            @endforeach
        </ul>
