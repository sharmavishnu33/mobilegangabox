<label for="inputState" class="grey">Talla:</label>
<select id="inputState" class="form-control color_size">
    <option>Select talla</option>
    @foreach($productSizes as $productSize)
    <option value="{{ $productSize->size }}">{{ $productSize->size }}</option>
        @endforeach
</select>