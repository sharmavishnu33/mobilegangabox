<option disabled selected>Select Branch</option>
@foreach($branches as $branch)
    <option value="{{ $branch->id }}" {{ Session::get('branch_id') == $branch->id ? 'selected' :'' }} data-url="{{ route('get.branch.product',$branch->name) }}">{{ $branch->name }}</option>
@endforeach
