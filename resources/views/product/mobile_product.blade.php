<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src='https://kit.fontawesome.com/a076d05399.js'></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/toastr.css') }}">
      <title>Gangabox | Product</title>
{{--       <style>--}}
{{--           .active,--}}
{{--           .red:hover {--}}
{{--               background-color: #ff7100;--}}
{{--               color: white;--}}
{{--           }--}}
{{--           .color .red {--}}
{{--               border: 2px solid #ff7100;--}}
{{--               padding: 5px;--}}
{{--               border-radius: 5px;--}}
{{--               font-size: 12px;--}}
{{--               font-weight: 600;--}}
{{--           }--}}
{{--       </style>--}}


       <style>
           .list .sele1 {
               width: 78px;
           }


           .list li {
               cursor: pointer;
               position: relative;
           }

           .header-right .list li img {
               width: 40px;
               margin-top: -5px;
               display: inherit;
               margin: 0 auto;
           }

           .counters_new {
               position: absolute;
               top: 0px;
               left: 38px;
               background: #fff;
               border: 1px solid #e16b1a;
               color: #e16b1a;
               border-radius: 50%;
               padding: 0px 5px;
           }
           .play_icon img {
               width: 70px !important;
           }
           .play_icon {
               position: absolute;
               top: 50%;
               left: 50%;
               transform: translate(-50%, -50%);
           }
           iframe {
               position: relative;
           }

       </style>
   </head>
   <body>


   <header class="header header-transparent">
       <div class="header-middle">
           <div class="container-fluid">
               <div class="row">
                   <div class="col-lg-4 col-4">
                       <div class="header-left" style="float: left;margin-top: 14px;">
                           <button class="mobile-menu-toggler" type="button">
                               <img src="{{asset('assets/img/mob-menu.png')}}">
                           </button>

                       </div>
                       <div class="d-lg-none d-block">
                           <div class="header-right">
                               <ul class="list">
                                   <li><img src="{{ asset('assets/img/Iconos y Banner-08.jpg') }}" class="inherit">
                                       <select class="currency-selector sele1 branches">
                                           <option selected>CDMX</option>
                                           <option >ABC</option>
                                       </select>
                                   </li>
                               </ul>
                           </div>
                       </div>

                   </div>
                   <div class="col-lg-4 col-4">
                       <a href="{{ url('/') }}" class="logo">
                           <img src="{{ asset('assets/img/logo.png') }}" alt="Logo">
                       </a>
                   </div>
                   <div class="col-lg-4 col-12 d-lg-block d-none">
                       <div class="header-right">
                           <ul class="list">
                               <li><img src="{{ asset('assets/img/Iconos y Banner-08.jpg') }}" class="inherit">
                                   <select class="currency-selector sele1 branches">
                                       <option selected>CDMX</option>
                                       <option >ABC</option>
                                   </select>
                               </li>
                               <li>
                                   <img src="{{ asset('assets/img/mobi.gif') }}" class="inherit">
                                   <select class="currency-selector sele2">
                                       <option selected>App</option>
                                       <option >ABC</option>
                                       <option >Management</option>
                                       <option >Business</option>
                                       <option >Gangabox</option>
                                   </select>
                               </li>
                               <li>
                                   <div id="counter" class="counters_new">
                                       <span class="count totalcart">{{ $carttotal->cartcount }}</span>
                                   </div>
                                   <a href="{{ route('cart.list') }}">  <img src="{{ asset('assets/img/2 icon gray.png') }}" ><p style="font-size: 17px;font-weight: bold;color: #949399;">Carrito</p></a>
                               </li>
                           </ul>
                       </div>
                   </div>
                   <div class="col-lg-4 col-4 d-lg-none d-block">
                       <div class="header-right">
                           <ul class="list">
                               <li>
                                   <div id="counter" class="counters">
                                       <span class="count totalcart">0</span>
                                   </div>
                                   <a href="{{ route('cart.list') }}">  <img src="{{ asset('assets/img/2 icon gray.png') }}" ><p style="font-size: 17px;font-weight: bold;color: #949399;">Carrito</p></a></li>
                           </ul>
                       </div>
                   </div>
               </div>

               <!-- End .header-left -->


               <!-- End .header-right -->
           </div>
           <!-- End .container-fluid -->
       </div>
       <!-- End .header-middle -->
{{--       <div class="top-cate">--}}
{{--           <?php $families  = \App\Http\Controllers\FamilyContoller::familyData();--}}
{{--           $i =0;?>--}}

{{--           @foreach($families as $family)--}}
{{--               <a><span class="familyData {{ $i==0 ?'active':'' }}" data-fid="{{$family->id}})">{{$family->name}}</span></a>--}}
{{--               <?php $i++; ?>--}}
{{--           @endforeach--}}
{{--       </div>--}}
   </header>
      <div class="container-fluid mt-5">
         <div class="row">
            <div class="col-12">
               <div class="product-single-container">
                  <div class="row">
                     <div class="product-single-gallery">
                        <div class="product-slider-container product-item">
                            <!-- slider -->
                           <div id="demo" class="carousel slide" data-ride="carousel">
                              <ul class="carousel-indicators">
                                  <?php $i =0;?>
                                  @foreach($varients as $productImg)
                                          @if($productImg->productImage->images)
                                 <li data-target="#demo" data-slide-to="{{$i}}" class="{{ $i ==0?'active':'' }} "></li>
                                          <?php $i++;?>
                                          @endif
                                 @endforeach
                                      @if($pvariant->video_link)
                                          <li data-target="#demo" data-slide-to="{{ $i+1 }}" class="{{ $i ==0?'active':'' }}"></li>
                                      @endif
                              </ul>
                              <div class="carousel-inner">
                                  <?php $j =0;?>
                                  @foreach($varients as $productImg)
                                          @if($productImg->productImage->images)
                                 <div class="carousel-item {{ $j ==0?'active':'' }}">
                                     @if(env('APP_ENV') == 'local')
                                        <img src="{{env('localUrl').'storage/images/'. $productImg->productImage->images }}" alt="banner" class="productimg" >
                                     @else
                                     <img src="{{env('serverUrl').'storage/images/'. $productImg->productImage->images }}" alt="banner" class="productimg" >
                                     @endif

                                    <!-- <div class="carousel-caption">
                                       <h3>Los Angeles</h3>
                                       <p>We had such a great time in LA!</p>
                                       </div>-->
                                 </div>
                                              <?php $j++;?>
                                          @endif
                                      @endforeach
                                      @if($pvariant->video_link)
                                      <div class="carousel-item">
{{--                                          <iframe width="560" height="315" src="https://www.youtube.com/embed/kJQP7kiw5Fk?&theme=dark&autohide=2&modestbranding=1&showinfo=0" frameborder="0"  allowfullscreen></iframe>--}}

                                          <iframe src="https://www.youtube.com/embed/{{$pvariant->video_id}}?autoplay=1&showinfo=0&modestbranding=1&wmode=transparent&controls=1&color=white&rel=0&enablejsapi=1&playsinline=1&&version=3&theme=light&autohide=1&egm=0&showsearch=0" frameborder="0" allowfullscreen></iframe>
{{--                                          <div class="play_icon">--}}
{{--                                              <img src="{{ asset('assets/img/PLAY ICON copy.png') }}" >--}}
{{--                                          </div>--}}
                                      </div>
                                          @endif

                              </div>
                              <a class="carousel-control-prev" href="#demo" data-slide="prev">
                              <span class="carousel-control-prev-icon"></span>
                              </a>
                              <a class="carousel-control-next" href="#demo" data-slide="next">
                              <span class="carousel-control-next-icon"></span>
                              </a>
                           </div>
                           <!-- slider -->
                           <!-- End .product-single -->
                           <span class="prod-full-screen">
                           <i class="icon-plus"></i>
                           </span>
                        </div>
                     </div>
                     <div class="col-12">
                        <div class="product-single-details">
                           <h1 class="product-title">{{ $pvariant->name }}</h1>
                           <div class="price-box">
                              <span class="product-price">MXN$<span class="sizePrice">{{ $pvariant->productVariant->product_price }}</span></span>
                           </div>
                           <!-- End .price-box -->
                           <div class="product-desc">
                               <ul>
                                   <li class="mb-2">Recíbelo mañana
                                   </li>
                                   <li
                                       class="mb-2">15 días de garantía
                                   </li>
                                   <li class="mb-2">Págalo en efectivo al recibir
                                   </li>
                                   <li class="mb-2">Cambios y devoluciones sin costo
                                   </li>
                               </ul>
                           </div>
                           <!-- End .product-desc -->

                        </div>
                        <!-- End .product-single-details -->
                     </div>
                     <!-- End .col-lg-5 -->
                     <div class="product-action mt-3">
                        <div class="product_price">
                           <span>MXN$<span class="sizePrice">{{ $pvariant->productVariant->product_price }}</span></span>
                           <span>recibelo manana</span>
                        </div>
                        <!-- End .product-single-qty -->
                        <a href="{{ route('cart.list') }}" class="paction add-cart addtocart" title="Add to Cart" >
                        <span><img class="crt" src="{{ asset('assets/img/inferior.png') }}"/></span>

                            <div id="counter" class="counter">
                                <span class="count totalcart">{{ $carttotal->cartcount }}</span>
                            </div>
                            <span>carrito</span>
                        </a>
                        <a  class="paction add-compare showvariant" title="Add to compare" data-url="{{ route('add.cart') }}"  data-pid="{{ $pvariant->id }}">
                        <span>comprar</span>
                        </a>
                     </div>
                     <!-- End .product-action -->
                  </div>
                  <!-- End .row -->
               </div>

                <div class="product-action clos variantsshow varient_bar" style="display: none">
                    <div class="display relate">
                        <div class="name">
                            @foreach($varients as $productColor)
                                @if ($loop->first)
                            <div class="box">

                                        <input type="hidden" value="{{ $productColor->colorData->id }}" id="color_name">
                                        <input type="hidden" value="{{ $pvariant->id }}" id="product_id">
                                        <input type="hidden" value="{{ $productColor->size }}" id="color_size">
                                            @if(env('APP_ENV') == 'local')
                                                <img src="{{env('localUrl').'storage/images/'. $productColor->productImage->images }}" width="100%" alt="img">
                                            @else
                                                <img src="{{env('serverUrl').'storage/images/'. $productColor->productImage->images }}" width="100%" alt="img">
                                            @endif



                            </div>
                                    <p class="product-price">MXN$<span class="sizePrice">{{ $productColor->product_price }}</span></p>
                            <p><b>Seleccion: AZUL</b></p>
                        </div>
                            @endif
                            @endforeach
{{--                        <div class="sticker">--}}
{{--                            <p>Quick<br>--}}
{{--                                guide</p>--}}
{{--                        </div>--}}
                        <i class="fas fa-close " id="hide"></i>
                    </div>

                    <div class="display">
{{--                        <div class="color" id="colornew">--}}
{{--                            <p><b>COLOR:&nbsp;</b>--}}
{{--                                <button class="red ">AZUL</button>&nbsp;--}}
{{--                                <button class="red activenew" >ROSA</button>&nbsp;--}}
{{--                                <button class="red">NEGRO</button>&nbsp;--}}
{{--                                <button class="red">PLATA</button>&nbsp;--}}
{{--                                <button class="red">ORO</button>--}}
{{--                            </p>--}}
{{--                        </div>--}}
                        <div class="color" id="colorname">
                            <p><b>COLOR:&nbsp;</b>
                                <?php $j=0; ?>
                                @foreach($colors as $productColor)

                                    <input type="hidden" id="colorId{{$productColor->colorData->id}}" value="{{ $productColor->colorData->id }}">
                                <span class="colordata {{ $j ==0 ?'active':''  }}" data-colorId="{{ $productColor->colorData->id }}">{{ $productColor->colorData->color_name }}</span>&nbsp;
                                    <?php $j++; ?>
                              @endforeach

                            </p>
                        </div>

                    </div>
                    <div class="display">
                        <div class="size color" id="size_element" data-url="{{ route('product.price') }}">

                        </div>
                    </div>

                    <div class="display">
                        <div class="product_price" style="text-align: center;">
                            <div class="number">
                                <span class="minus">-</span>
                                <input type="text" id="quantity" name="p_qnt" value="1"/>
                                <span class="plus">+</span>
                            </div>
                        </div>
                        <div class="carito">
                            <a href="{{ route('cart.list') }}" title="Add to Cart" >
                                <span><img src="{{ asset('assets/img/inferior.png') }}" class="pt-2" width="22px"></span>
                                <div  class="counter">
                                    <span class="count totalcart">{{ $carttotal->cartcount }}</span>
                                </div>
                                <p class="m-0"><b>Carrito</b></p>
                            </a>
                        </div>
                        <a class="paction add-compare addtocart"  data-url="{{ route('add.cart') }}"  data-pid="{{ $pvariant->id }}" title="Add to compare">
                            <span>comprar</span>
                        </a>
                        <!-- <div class="product_price">
                           <span>MXN$99</span>
                           <span>recibelo manana</span>
                        </div> -->
                    </div>

                    <!-- End .product-single-qty -->


                </div>

               <!-- End .product-single-container -->
               <div class="product-single-tabs row">
                  <ul class="nav nav-tabs" role="tablist">
                     <li class="nav-item">
                        <a class="nav-link active" id="product-tab-desc" data-toggle="tab" href="#product-desc-content" role="tab" aria-controls="product-desc-content" aria-selected="true">Description</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" id="product-tab-spec" data-toggle="tab" href="#product-spec-content" role="tab" aria-controls="product-spec-content" aria-selected="false">specification</a>
                     </li>
                  </ul>
                  <div class="tab-content">

                     <div class="tab-pane fade show active" id="product-desc-content" role="tabpanel" aria-labelledby="product-tab-desc">
                        <div class="product-desc-content">
                           <?php echo $pvariant->description;?>
                              @foreach($productImage as $pimage)
                                  @if(env('APP_ENV') == 'local')
                                   <img src="{{ env('localUrl').'storage/images/'. $pimage->image }}" class="img-fluid">
                                   @else
                                   <img src="{{ env('serverUrl').'storage/images/'. $pimage->image }}" class="img-fluid">
                                   @endif
                                   @endforeach
                        </div>
                        <!-- End .product-desc-content -->
                     </div>
                     <!-- End .tab-pane -->
                     <div class="tab-pane fade" id="product-spec-content" role="tabpanel" aria-labelledby="product-tab-spec">
                        <div class="product-spec-content">
                           <?php echo $pvariant->specification;?>
                        </div>
                        <!-- End .product-spec-content -->
                     </div>
                     <!-- End .tab-pane -->
                  </div>
                  <!-- End .tab-content -->
               </div>
               <!-- End .product-single-tabs -->
            </div>
         </div>
      </div>



   <div class="mobile-menu-overlay"></div>
   <!-- End .mobil-menu-overlay -->
   <div class="mobile-menu-container" >
       <div class="mobile-menu-wrapper">
           <div class="menu-top"> <span><img src="{{ asset('assets/img/logo.png') }}"></span><span class="mobile-menu-close"><img src="{{ asset('assets/img/close.png') }}"></span></div>
           <nav class="mobile-nav" id="submenu">
               <ul class="mobile-menu">
                   <li class="dropdown">
                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="caret">Categorías</span></a>
                       <ul class="dropdown-menu">
                           <li><a href="listing.html">submenusdedwed</a></li>
                           <li><a href="listing.html">submenu</a></li>
                           <li><a href="listing.html">submenu</a></li>
                       </ul>
                   </li>
                   <li>
                       <a href="listing.html">Más vendido</a>
                   </li>

               </ul>
           </nav>
           <!-- End .mobile-nav -->
       </div>
       <!-- End .mobile-menu-wrapper -->
   </div>
       <!-- popup -->
       <div class="overlayer" style="display: none;">
         <div class="popup">
            <div class="login">
               <section >
                  <div class="header-middle">
                     <div class="container">
                        <div class="row align-items-center">
                           <div class="col-4">
                              <div class="header-left">
                                 <i class='fas fa-times ml-2 ' id="hide" style="font-size: 20px;"></i>
                              </div>
                           </div>
                           <div class="col-8">
                              <a href="index.html" class="logo">
                                 <img src="assets/img/logo.png" alt="Logo">
                                 </a>
                           </div>
                        </div>

                        <!-- End .header-left -->

                        <!-- End .header-right -->
                     </div>
                     <!-- End .container-fluid -->
                  </div>
                 <!-- End .header-middle -->
               </section>
               <div class="container-fluid mt-3">

                  <div class="row">
                     <div class="col-12">
                         <ul class="nav nav-tabs tab_1">
                             <li class="active"><a data-toggle="tab" href="#home">INGRESAR</a></li>
                             <li><a data-toggle="tab" href="#menu1">REGISTRARSE</a></li>
                           </ul>

                           <div class="tab-content">
                             <div id="home" class="tab-pane in active">
                                 <form class="form">
                                     <div class="row">
                                         <div class="col-12">
                                             <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="Correo electronico">
                                         </div>
                                         <div class="col-12">
                                             <div class="input-group mb-2 mr-sm-2">
                                             <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Contrasena">
                                             </div>
                                         </div>

                                     </div>
                                     <div class="row my-3">
                                         <div class="col-12 text-center">
                                             <a href="#" class="orange">Iniciar sesion</a>
                                         </div>
                                     </div>
                                 </form>
                             </div>
                             <div id="menu1" class="tab-pane">
                                 <form class="form">
                                     <div class="row">
                                         <div class="col-12">
                                             <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="Correo electronico">
                                         </div>
                                         <div class="col-12">
                                             <div class="input-group mb-2 mr-sm-2">
                                             <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Contrasena">
                                             </div>
                                         </div>

                                     </div>
                                     <div class="row my-3">
                                         <div class="col-12 text-center">
                                             <a href="#" class="orange">REGISTRARSE</a>
                                         </div>
                                     </div>
                                 </form>
                             </div>

                           </div>

                           <div class="row my-3">
                             <div class="col-12 text-center line">
                                 <p><span class="pull-left"></span>o<span class="pull-right"></span></p>
                             </div>
                         </div>
                         <div class="row my-3">
                             <div class="col-12 text-center">
                                 <a href="#" class="facebook"><i class="fab fa-facebook-square"></i> Iniciar sesion con Facebook</a>
                             </div>
                         </div>
                         <div class="row my-3">
                             <div class="col-12 text-center">
                                 <a href="#" class="gmail">
                                     <img src="assets/img/gmail.png" width="30px" style="margin-top: -6px;">
                                     <span>Iniciar sesion con Gmail</span>
                                 </a>
                             </div>
                         </div>
                         <div class="row my-3">
                             <div class="col-12 text-center grey">
                                 <p>AI inciar sesion usted acepta <br> los <span class="red"><a href="#">treminos y condiciones y politicas de privacidad</a></span> </p>
                             </div>
                         </div>
                     </div>
                  </div>
               </div>
            </div>

         </div>
      </div>
   <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
   <script src="{{ asset('assets/js/popper.min.js') }}"></script>
   <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
   <script src="{{ asset('assets/js/number.js') }}"></script>
   <script src="{{ asset('assets/js/toastr.min.js') }}"></script>
<script>
    updateCart();
    function updateCart() {
        $.ajax(
            {
                url: ' {{ route('get.details') }} ',
                type: 'get',
                success: function (result) {
                    if (result.success) {
                        $('.branches').html('');
                        $('.count').text(result.cartTotal.cartcount);
                        // var mapData = JSON.parse(result.data);
                        $('.branches').html(result.data);
                    } else {
                        return false;
                    }
                }
            });
    }
    var colorid = $('#color_name').val();
    var product_id = $('#product_id').val();
    $.ajax(
        {
            url: ' {{ route('color.size') }} ',
            type: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                "color_id": colorid,
                "product_id": product_id,
            },
            success: function (result) {
                if (result.success) {
                    $('#size_element').html('');
                    // var mapData = JSON.parse(result.data);
                    $('#size_element').html(result.data);
                } else {
                    return false;
                }
            }
        });

    $('.colordata').off('click').on('click',function () {
        var _that = $(this);
        var colorid = _that.attr('data-colorId');
         $('#color_name').val(colorid);
        var product_id = $('#product_id').val();
        $(".colordata").removeClass("active");
        $(this).addClass("active");
        $.ajax(
            {
                url: '{{ route('color.size') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    "color_id": colorid,
                    "product_id": product_id,
                },
                success: function (result) {
                    if (result.success) {
                        $('#size_element').html('');
                        $('.sizePrice').text('');
                        // var mapData = JSON.parse(result.data);
                        $('#size_element').html(result.data);
                        $('.sizePrice').text(result.price_data);
                    } else {
                        return false;
                    }
                }
            });
    })

</script>
      <script>
               $('.addtocart').off('click').on('click',function (e) {
        var _that = $(this);
        var _url = _that.attr('data-url');
        var productId = _that.attr('data-pid');
        var colorid = $('#color_name').val();
        var colorsize = $('#color_size').val();
        var quanti = $('#quantity').val();
        if(colorid == 'Select Color' )
        {
            toastr.error('Please select color');
            return false;
        }
        if(colorsize == 'Select talla' || colorsize =='Select size'){
            toastr.error('Please select size');
            return false;
        }
        $.ajax(
            {
                url: _url,
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    "colorId": colorid,
                    "colorSize": colorsize,
                    "quanti": quanti,
                    "productId": productId,
                },
                success: function (result) {
                    if (result.success) {
                        console.log(result.cartNumber)
                        $('.totalcart').text(result.cartNumber);
                        toastr.success('Cart add successfully');

                        // $('.size_details').html('');
                        // // var mapData = JSON.parse(result.data);
                        // $('.size_details').html(result.data);
                    }
                    else {
                            toastr.error('You can add only '+result.quantity+' quantity for this .');
                            return false;
                    }
                }
            });
    });
$('.showvariant').off('click').on('click',function () {
    $('.variantsshow').css('display', 'block');
})
//                $('.remove-compare').off('click').on('click',function () {
//     $('.variantsshow').css('display', 'none');
// })

//                $('.colordata').off('click').on('click',function () {
//                    var _that = $(this);
//                    var colorId = _that.attr('data-colorId');
//                    var colorsize = _that.attr('data-colorsize');
//                    $('#color_name').val(colorId);
//                    $('#color_size').val(colorsize);
// })

</script>
   <script>
       getFamilySub();
       function getFamilySub()
       {
           $.ajax(
               {
                   url: ' {{ route('get.family.sub') }} ',
                   type: 'get',
                   success: function (result) {
                       if (result.success) {
                           $('#submenu').html('');
                           // $('.count').text(result.cartTotal.cartcount);
                           // var mapData = JSON.parse(result.data);
                           $('#submenu').html(result.data);
                       } else {
                           return false;
                       }
                   }
               });
       }
   </script>
   <script>
       $('#size_element').off('click').on('click',function () {
           var sizeValue = $(this).find('span.active').text();
           var _url = $(this).attr('data-url');
           var product_id = $('#product_id').val();
           var colorid = $('#color_name').val();
           // var colorsize = $('#color_size').val();
           // alert(colorid);
           $.ajax(
               {
                   url: _url,
                   type: 'POST',
                   data: {
                       _token: "{{ csrf_token() }}",
                       "colorid": colorid,
                       "productId": product_id,
                       "size": sizeValue,
                   },
                   success: function (result) {
                       if (result.success) {
                           $('.sizePrice').html('');
                           // var mapData = JSON.parse(result.data);
                           $('.sizePrice').html(result.data);
                       } else {
                           return false;
                       }
                   }
               });

       })
   </script>
{{--      <script>--}}
{{--          var header = $("#colornew").val();--}}
{{--          var btns = $(header+".red");--}}
{{--          alert(btns.length);--}}
{{--          for (var i = 0; i < btns.length; i++) {--}}
{{--              btns[i].addEventListener("click", function() {--}}
{{--                  var current = $(".active").val();--}}
{{--                  current[0].className = current[0].className.replace(" active", "");--}}
{{--                  this.className += " active";--}}
{{--              });--}}
{{--          }--}}
{{--      </script>--}}
