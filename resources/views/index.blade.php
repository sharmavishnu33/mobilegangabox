@extends('layout.layout')
@section('content')
      <!-- End .header -->
      <!-- end -->

      <!-- slider -->
      <section class="mobile slider ">
      <div id="demo" class="carousel slide " data-ride="carousel" >
         <ul class="carousel-indicators">
             <?php $k=0 ?>
             @foreach($banners as $banner)
                 <li data-target="#demo" data-slide-to="{{ $k }}" class="{{ $k==0 ?'active':'' }}"></li>
                 <?php $k++; ?>
             @endforeach
         </ul>
         <div class="carousel-inner">
             <?php $l=0 ?>
             @foreach($banners as $banner)
                 <div class="carousel-item {{ $l== 0 ? 'active':'' }}">
@if(env('APP_ENV') == 'local')
                     <img src="{{env('localUrl').'storage/images/'.$banner->images ? env('localUrl').'storage/images/'.$banner->images : 'http://gangabox.test/storage/images/'.$banner->images }}" width="100%" height="300">
                     @else
                     <img src="{{env('serverUrl').'storage/images/'.$banner->images ? env('serverUrl').'storage/images/'.$banner->images : 'http://gangabox.test/storage/images/'.$banner->images }}" width="100%" height="300">
    @endif
                 </div>
                 <?php $l++; ?>
             @endforeach

         </div>
         <a class="carousel-control-prev" href="#demo" data-slide="prev">
         <span class="carousel-control-prev-icon"></span>
         </a>
         <a class="carousel-control-next" href="#demo" data-slide="next">
         <span class="carousel-control-next-icon"></span>
         </a>
      </div>

   </section>
      <!-- slider -->
      <!-- sidebar  -->
      <!-- sidebar  -->
  <!-- sidebar  -->

      <!-- popup -->

      <!-- popup -->
<input type="hidden" id="lastId" value="{{ $lastProductId }}">
<input type="hidden" id="previousID">
      <section class="container-fluid item-product scrolldown ">
          <div class="row row-sm fproductlist">
              @foreach($products as $product)
                  <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
                  <div class="col-6 col-md-4 col-lg-3 col-xl-2">

                      <div class="product-default">

                          <figure>
                              <a href="{{ route('product.description',$productId) }}">
                                  @if(env('APP_ENV') == 'local')
                                  <img class="lazy" src="{{URL::asset('assets/img/PRELOADER.jpg') }}"
                                       data-src="{{env('localUrl').'storage/images/'.$product->images}}" width="100">
                                  @else
                                  <img class="lazy" src="{{URL::asset('assets/img/PRELOADER.jpg') }}"
                                       data-src="{{env('serverUrl').'storage/images/'.$product->images}}" width="100">
                                      @endif
                              </a>
                          </figure>
                          <div class="product-details">
                              <h2 class="product-title">
                                  <a href="">{{$product->product_name}}</a>
                              </h2>
                              <div class="price-box">
                                  <span class="product-price"><i>${{$product->product_price}}</i> MXN</span>
                              </div>
                              <!-- End .price-box -->
                              <div class="category-wrap">
                                  <div class="category-list">
                                      <a href="" class="product-category">Recibelo Mañana</a>
                                  </div>
                              </div>
                          </div>
                          <!-- End .product-details -->

                      </div>

                  </div>
              @endforeach
          </div>

{{--          <div class="overlayer">--}}
{{--              <div class="popup">--}}
{{--                  <h4>Pais</h4>--}}
{{--                  <Ul>--}}
{{--                      <li><i class="icon"></i>Mexico<i class="fa fa-angle-right"></i></li>--}}
{{--                      <li><i class="icon ic"></i>Colombia<i class="fa fa-angle-right"></i></li>--}}
{{--                      <li><i class="icon ic-1"></i>Brazil<i class="fa fa-angle-right"></i></li>--}}
{{--                  </Ul>--}}
{{--              </div>--}}
{{--          </div>--}}
      </section>
      <div class="filter-overlay"></div>
     @endsection
