<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <title>Home</title>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>

    <style>
        .list .sele1 {
            width: 78px;
        }


        .list li {
            cursor: pointer;
            position: relative;
        }

        .header-right .list li img {
            width: 40px;
            margin-top: -5px;
            display: inherit;
            margin: 0 auto;
        }

        .counters_new {
            position: absolute;
            top: 0px;
            left: 38px;
            background: #fff;
            border: 1px solid #e16b1a;
            color: #e16b1a;
            border-radius: 50%;
            padding: 0px 5px;
        }
        .inherit {
            display: inherit;
            margin: 0 auto;
            width: 50px;
        }
    </style>

</head>
<body class="index">

<header class="header header-transparent">
    <div class="header-middle">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-4">
                    <div class="header-left" style="float: left;margin-top: 14px;">
                        <button class="mobile-menu-toggler" type="button">
                            <img src="{{asset('assets/img/mob-menu.png')}}">
                        </button>

                    </div>
                    <div class="d-lg-none d-block">
                        <div class="header-right">
                            <ul class="list">
                                <li><img src="{{ asset('assets/img/Iconos y Banner-08.jpg') }}" class="inherit">
                                    <select class="currency-selector sele1 branches">
                                        <option selected>CDMX</option>
                                        <option >ABC</option>
                                    </select>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
                <div class="col-lg-4 col-4">
                    <a href="{{ url('/') }}" class="logo">
                        <img src="{{ asset('assets/img/logo.png') }}" alt="Logo">
                    </a>
                </div>
                <div class="col-lg-4 col-12 d-lg-block d-none">
                    <div class="header-right">
                        <ul class="list">
                            <li><img src="{{ asset('assets/img/Iconos y Banner-08.jpg') }}" class="inherit">
                                <select class="currency-selector sele1 branches">
                                    <option selected>CDMX</option>
                                    <option >ABC</option>
                                </select>
                            </li>
                            <li>
                                <img src="{{ asset('assets/img/mobi.gif') }}" class="inherit">
                                <select class="currency-selector sele2">
                                    <option selected>App</option>
                                    <option >ABC</option>
                                    <option >Management</option>
                                    <option >Business</option>
                                    <option >Gangabox</option>
                                </select>
                            </li>
                            <li>
                                <div id="counter" class="counters_new">
                                    <span class="count totalcart">{{ $carttotal->cartcount }}</span>
                                </div>
                                <a href="{{ route('cart.list') }}">    <img src="{{ asset('assets/img/2 icon gray.png') }}" ><p style="font-size: 17px;font-weight: bold;color: #949399;">Carrito</p></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-4 d-lg-none d-block">
                    <div class="header-right">
                        <ul class="list">
                            <li>
                                <div id="counter" class="counters">
                                    <span class="count totalcart">0</span>
                                </div>
                                <a href="{{ route('cart.list') }}">  <img src="{{ asset('assets/img/2 icon gray.png') }}" ><p style="font-size: 17px;font-weight: bold;color: #949399;">Carrito</p></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- End .header-left -->


            <!-- End .header-right -->
        </div>
        <!-- End .container-fluid -->
    </div>
    <!-- End .header-middle -->
    <div class="top-cate">

    </div>
</header>

<div class="overlayer" style="display: none;">
    <div class="popup">
        <div class="login">
            <section >
                <div class="header-middle">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-4">
                                <div class="header-left">
                                    <i class='fas fa-times ml-2 ' id="hide" style="font-size: 20px;"></i>
                                </div>
                            </div>
                            <div class="col-8">
                                <a href="{{ url('/') }}" class="logo">
                                    <img src="{{ asset('assets/img/logo.png') }}" alt="Logo">
                                </a>
                            </div>
                        </div>

                        <!-- End .header-left -->

                        <!-- End .header-right -->
                    </div>
                    <!-- End .container-fluid -->
                </div>
                <!-- End .header-middle -->
            </section>
            <div class="container-fluid mt-3">

                <div class="row">
                    <div class="col-12">
                        <ul class="nav nav-tabs tab_1">
                            <li class="active"><a data-toggle="tab" href="#home">INGRESAR</a></li>
                            <li><a data-toggle="tab" href="#menu1">REGISTRARSE</a></li>
                        </ul>

                        <div class="tab-content">
                            <div id="home" class="tab-pane in active">
                                <form class="form">
                                    <div class="row">
                                        <div class="col-12">
                                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="Correo electronico">
                                        </div>
                                        <div class="col-12">
                                            <div class="input-group mb-2 mr-sm-2">
                                                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Contrasena">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row my-3">
                                        <div class="col-12 text-center">
                                            <a href="#" class="orange">Iniciar sesion</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="menu1" class="tab-pane">
                                <form class="form">
                                    <div class="row">
                                        <div class="col-12">
                                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="Correo electronico">
                                        </div>
                                        <div class="col-12">
                                            <div class="input-group mb-2 mr-sm-2">
                                                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Contrasena">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row my-3">
                                        <div class="col-12 text-center">
                                            <a href="#" class="orange">REGISTRARSE</a>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                        <div class="row my-3">
                            <div class="col-12 text-center line">
                                <p><span class="pull-left"></span>o<span class="pull-right"></span></p>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col-12 text-center">
                                <a href="#" class="facebook"><i class="fab fa-facebook-square"></i> Iniciar sesion con Facebook</a>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col-12 text-center">
                                <a href="#" class="gmail">
                                    <img src="assets/img/gmail.png" width="30px" style="margin-top: -6px;">
                                    <span>Iniciar sesion con Gmail</span>
                                </a>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col-12 text-center grey">
                                <p>AI inciar sesion usted acepta <br> los <span class="red"><a href="#">treminos y condiciones y politicas de privacidad</a></span> </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>



