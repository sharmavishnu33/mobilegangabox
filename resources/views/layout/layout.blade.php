@include('element.header')
@yield('content')

<div class="mobile-menu-overlay"></div>
<!-- End .mobil-menu-overlay -->
<div class="mobile-menu-container" >
    <div class="mobile-menu-wrapper">
        <div class="menu-top"> <span><img src="{{ asset('assets/img/logo.png') }}"></span><span class="mobile-menu-close"><img src="{{ asset('assets/img/close.png') }}"></span></div>
        <nav class="mobile-nav" id="submenu">
            <ul class="mobile-menu">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="caret">Categorías</span></a>
                    <ul class="dropdown-menu">
                        <li><a href="listing.html">submenusdedwed</a></li>
                        <li><a href="listing.html">submenu</a></li>
                        <li><a href="listing.html">submenu</a></li>
                    </ul>
                </li>
                <li>
                    <a href="listing.html">Más vendido</a>
                </li>

            </ul>
        </nav>
        <!-- End .mobile-nav -->
    </div>
    <!-- End .mobile-menu-wrapper -->
</div>
<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-12">
                <p>Rasterio de Predidos y dudas sobre nuestros articulos</p>
                <p>Mensaje de WhatsApp: 55 8732<br>2760</p>
            </div>
            <div class="col-lg-6 col-12">
                <img src="{{ asset('assets/img/app-store.png') }}" width="150px">
                <img src="{{ asset('assets/img/google.png') }}" width="150px">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6"></div>
            <div class="col-lg-6">
                <p>Menu Inferior</p>
                <ul>
                    <li><a href="#">Busquesda</a></li>
                    <li><a href="#">Preguntas Frecuentes</a></li>
                    <li><a href="#">Politica de privacidad</a></li>
                    <li><a href="#">Envio Y devoluciones</a></li>
                    <li><a href="#">Quiesnes somos</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p>Atencion Al Cliente (Cambios)</p>
                <p>Mensaje de WhatsApp: 55 8029<br>8963</p>
            </div>
        </div>
    </div>
</footer>
<!-- footer  -->
<div class="app-noti">
    <a><i class="fa fa-times" aria-hidden="true"></i></a><span><img src="{{ asset('assets/img/app-logo.png') }}"></span>
    <div class="">
        <h4>Gangabox App</h4>
        <span class="sml">Descuentos exclusivos! Solo para app</span><span><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i> (31,046)</span>
    </div>
    <span><a href="" class="app-btn">descargar</a></span>
</div>

<!-- End .mobile-menu-container -->
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/number.js') }}"></script>
<script src="{{ asset('assets/js/toastr.min.js') }}"></script>
<script>
    updateCart();
    getFamilySub();
    function updateCart() {
        $.ajax(
            {
                url: ' {{ route('get.details') }} ',
                type: 'get',
                success: function (result) {
                    if (result.success) {
                        $('.branches').html('');
                        $('.count').text(result.cartTotal.cartcount);
                        // var mapData = JSON.parse(result.data);
                        $('.branches').html(result.data);
                    } else {
                        return false;
                    }
                }
            });
    }

    function getFamilySub()
    {
        $.ajax(
            {
                url: ' {{ route('get.family.sub') }} ',
                type: 'get',
                success: function (result) {
                    if (result.success) {
                        $('#submenu').html('');
                        // $('.count').text(result.cartTotal.cartcount);
                        // var mapData = JSON.parse(result.data);
                        $('#submenu').html(result.data);
                    } else {
                        return false;
                    }
                }
            });
    }
    $('.familyData').off('click').on('click',function () {
        var _that = $(this);
        var categoryId = _that.attr('data-fid');
        $(".familyData").removeClass("active");
        $(this).addClass("active");
        $('#p_id').empty();
        $.ajax({
            _token: "{{ csrf_token() }}",
            url: "{{ route('get.family') }}",
            type: "POST",
            data: {id : categoryId, _token: "{{ csrf_token() }}"},
            success: function(response) {
                $('.fproductlist').html();
                $('.fproductlist').html(response.data);
            }
        });
    })

    $('.branches').off('change').on('change',function () {
        var _that = $(this);
        var branch = $('.branches').val();
        var url = $(this).children(":selected").data("url");
        // var _url = _that.attr('data-url');
        window.location = url;
    })

</script>
<script>
    $(document).ready(function () {
        scrollerData();

    });
</script>
<script>
    $('.filter span').on('click', function (e) {
        $('body').toggleClass('mmenu-active');
        $(this).toggleClass('active');
        e.preventDefault();
    });

    $('.filter-overlay, .close').on('click', function (e) {
        $('body').removeClass('mmenu-active');
        e.preventDefault();
    });
</script>
<script>
    window.onscroll = function() {myFunction()};

    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }
</script>
<script>
    updateCart();
    function updateCart() {
        $.ajax(
            {
                url: ' {{ route('get.details') }} ',
                type: 'get',
                success: function (result) {
                    if (result.success) {
                        $('.branches').html('');
                        $('.count').text(result.cartTotal.cartcount);
                        // var mapData = JSON.parse(result.data);
                        $('.branches').html(result.data);
                    } else {
                        return false;
                    }
                }
            });
    }
    var colorid = $('#color_name').val();
    var product_id = $('#product_id').val();
    $.ajax(
        {
            url: ' {{ route('color.size') }} ',
            type: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                "color_id": colorid,
                "product_id": product_id,
            },
            success: function (result) {
                if (result.success) {
                    $('#size_element').html('');
                    // var mapData = JSON.parse(result.data);
                    $('#size_element').html(result.data);
                } else {
                    return false;
                }
            }
        });

    $('.colordata').off('click').on('click',function () {
        var _that = $(this);
        var colorid = _that.attr('data-colorId');
        var product_id = $('#product_id').val();
        $(".colordata").removeClass("active");
        $(this).addClass("active");
        $.ajax(
            {
                url: '{{ route('color.size') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    "color_id": colorid,
                    "product_id": product_id,
                },
                success: function (result) {
                    if (result.success) {
                        $('#size_element').html('');
                        // var mapData = JSON.parse(result.data);
                        $('#size_element').html(result.data);
                    } else {
                        return false;
                    }
                }
            });
    })

</script>
<script>
    $('.addtocart').off('click').on('click',function (e) {
        var _that = $(this);
        var _url = _that.attr('data-url');
        var productId = _that.attr('data-pid');
        var colorid = $('#color_name').val();
        var colorsize = $('#color_size').val();
        var quanti = $('#quantity').val();
        if(colorid == 'Select Color' )
        {
            toastr.error('Please select color');
            return false;
        }
        if(colorsize == 'Select talla' || colorsize =='Select size'){
            toastr.error('Please select size');
            return false;
        }
        $.ajax(
            {
                url: _url,
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    "colorId": colorid,
                    "colorSize": colorsize,
                    "quanti": quanti,
                    "productId": productId,
                },
                success: function (result) {
                    if (result.success) {
                        console.log(result.cartNumber)
                        $('.totalcart').text(result.cartNumber);
                        toastr.success('Cart add successfully');

                        // $('.size_details').html('');
                        // // var mapData = JSON.parse(result.data);
                        // $('.size_details').html(result.data);
                    }
                    else {
                        toastr.error('You can add only '+result.quantity+' quantity for this .');
                        return false;
                    }
                }
            });
    });
    $('.showvariant').off('click').on('click',function () {
        $('.variantsshow').css('display', 'block');
    })
    //                $('.remove-compare').off('click').on('click',function () {
    //     $('.variantsshow').css('display', 'none');
    // })

    //                $('.colordata').off('click').on('click',function () {
    //                    var _that = $(this);
    //                    var colorId = _that.attr('data-colorId');
    //                    var colorsize = _that.attr('data-colorsize');
    //                    $('#color_name').val(colorId);
    //                    $('#color_size').val(colorsize);
    // })

</script>
<script>
    function scrollerData() {

        !function(window){
            var $q = function(q, res){
                    if (document.querySelectorAll) {
                        res = document.querySelectorAll(q);
                    } else {
                        var d=document
                            , a=d.styleSheets[0] || d.createStyleSheet();
                        a.addRule(q,'f:b');
                        for(var l=d.all,b=0,c=[],f=l.length;b<f;b++)
                            l[b].currentStyle.f && c.push(l[b]);

                        a.removeRule(0);
                        res = c;
                    }
                    return res;
                }
                , addEventListener = function(evt, fn){
                    window.addEventListener
                        ? this.addEventListener(evt, fn, false)
                        : (window.attachEvent)
                        ? this.attachEvent('on' + evt, fn)
                        : this['on' + evt] = fn;
                }
                , _has = function(obj, key) {
                    return Object.prototype.hasOwnProperty.call(obj, key);
                }
            ;

            function loadImage (el, fn) {
                var img = new Image()
                    , src = el.getAttribute('data-src');
                img.onload = function() {
                    if (!! el.parent)
                        el.parent.replaceChild(img, el)
                    else
                        el.src = src;

                    fn? fn() : null;
                }
                img.src = src;
            }

            function elementInViewport(el) {
                var rect = el.getBoundingClientRect()

                return (
                    rect.top    >= 0
                    && rect.left   >= 0
                    && rect.top <= (window.innerHeight || document.documentElement.clientHeight)
                )
            }

            var images = new Array()
                , query = $q('img.lazy')
                , processScroll = function(){
                    for (var i = 0; i < images.length; i++) {
                        if (elementInViewport(images[i])) {
                            loadImage(images[i], function () {
                                images.splice(i, i);
                            });
                        }
                    };
                }
            ;
            // Array.prototype.slice.call is not callable under our lovely IE8
            for (var i = 0; i < query.length; i++) {
                images.push(query[i]);
            };

            processScroll();
            addEventListener('scroll',processScroll);

        }(this);

    }
</script>

<script>
    var scroller = true;
    $(document).ready(function () {
        var i = 2;
        $(window).scroll(function () {
            if ($(window).scrollTop() >= $('.scrolldown').offset().top + $('.scrolldown').outerHeight() - window.innerHeight) {
// ajax call get data from server and append to the div
// if (scroller) {
                var rout = '{!! Route::currentRouteName() !!}';
                var lastId = $('#lastId').val();
                var previousID = $('#previousID').val();
// alert(rout);
                if (lastId) {
                    if (previousID != lastId) {
                        $('#previousID').val(lastId);
                        $.ajax(
                            {
                                url: '{{ route('get.more.data') }}',
                                type: 'GET',
                                data: {
                                    "page": i,
                                    'currentriute': rout,
                                    'lastId': lastId
                                },
                                success: function (result) {
                                    if (result.success) {
// $('.productslisting').html();

// toastr.success('Cart updated successfully');
                                        $('.fproductlist').append(result.data);
                                        $('#lastId').val(result.lastProductId);

                                    } else {
                                        return false;
                                    }
                                }
                            });
                        i = parseInt(i) + 1;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
// }
        });
    });
    // function scrollerupdate() {
    //     scroller = false;
    // }
</script>
</body>
</html>
