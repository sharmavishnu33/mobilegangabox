<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index')->name('home');
Route::post('/get_family','FamilyContoller@mobileProduct')->name('get.family');
Route::get('/product_list','product\ProductController@index')->name('product.list');
Route::get('/product_description/{id}','product\ProductController@productDescription')->name('product.description');
Route::post('/add-cart','product\ProductController@addCart')->name('add.cart');
Route::get('/cart','cart\CartController@index')->name('cart.list');
Route::post('/guest_confirm','cart\CartController@guestConfirm')->name('guest.confirm');
Route::post('/color-size','cart\CartController@colorSize')->name('color.size');
Route::post('/product-price','cart\CartController@productPrice')->name('product.price');

Route::post('/update_cart','cart\CartController@updateCart')->name('update.cart');
Route::post('/remove_cart','cart\CartController@removeCart')->name('remove.cart');
Route::post('/guest_confirm','cart\CartController@guestConfirm')->name('guest.confirm');
Route::post('/order_confirm','cart\CartController@orderConfirm')->name('order.confirm');

Route::get('/agree-confirm','cart\CartController@agreeConfirm')->name('agree.confirm');
Route::post('/update-confirm','cart\CartController@updateConfirm')->name('update.confirm');
Route::get('/thanks','cart\CartController@thanksPage')->name('thanks.page');
Route::get('/get-details','cart\CartController@getDetails')->name('get.details');
Route::get('get_branch_product/{name}','FrontController@branchProductList')->name('get.branch.product');
Route::get('get-family-sub','FrontController@getFamilySub')->name('get.family.sub');
Route::get('family-product-list/{sub}/{fid}','FrontController@familyProductList')->name('family.product.list');

Route::get('get-more-data','FrontController@getMoreData')->name('get.more.data');
