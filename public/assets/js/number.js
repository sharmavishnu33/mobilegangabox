$(document).ready(function () {
  $(".minus").click(function () {
    var $input = $(this).parent().find("input");
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });
  $(".plus").click(function () {
    var $input = $(this).parent().find("input");
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
  });
});
$(document).ready(function(){
    $("#hide").click(function(){
      $(".clos").hide();
      $(".overlay").hide();
      $(".overlayer").hide();
    });
    
  });
  $('.mobile-menu-toggler').on('click', function (e) {
    $('body').toggleClass('mmenu-active');
    $(this).toggleClass('active');
    e.preventDefault();
    });
    
    $('.mobile-menu-overlay, .mobile-menu-close').on('click', function (e) {
    $('body').removeClass('mmenu-active');
    e.preventDefault();
    });

    // tab 
    function openCity(evt, cityName) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      document.getElementById(cityName).style.display = "block";
      evt.currentTarget.className += " active";
    }
    
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    // account click 
    $(document).ready(function(){
      $(".acco").click(function(){
        $(".overlayer").toggle();
      });
    });
